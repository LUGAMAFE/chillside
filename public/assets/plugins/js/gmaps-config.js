var styleArray = 
	[{
		stylers: [
			{ hue: "#c31421" },
			{ saturation: "-20" },
			{ visibility: "simplified" }
		]
	}];
	function cargar_mapa() {
		var myLatlng = new google.maps.LatLng(20.9989949,-89.611442);
		var imagePath = "../../img/imagenes/angolo.png";
		var mapOptions = {
			zoom: 19,
			scrollwheel: false,
			center: myLatlng,
			styles: styleArray,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		}
		var map = new google.maps.Map(document.getElementById('map'), mapOptions);
		var contentString = 'Samline';
		var infowindow = new google.maps.InfoWindow({
			content: contentString,
			maxWidth: 500
		});
		var marker = new google.maps.Marker({
			position: myLatlng,
			map: map,
			icon: imagePath,
			title: 'image title'
		});
		google.maps.event.addListener(marker, 'click', function() {
			infowindow.open(map,marker);
		});
		google.maps.event.addDomListener(window, "resize", function() {
			var center = map.getCenter();
			google.maps.event.trigger(map, "resize");
			map.setCenter(center);
		});
	}