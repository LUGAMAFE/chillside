<script>
	$(document).ready(function(){

        $(".btnCancelar").click(function(){
            let productos = $(".producto");

            $(this).parent().parent().parent().remove();
        });

        $(".sumadorProductos").each(function (index, element) {
            let sumador = element;
            let menos = $(sumador).find(".menos");
            let mas = $(sumador).find(".mas");
            let cantidad = $(sumador).find(".cantidad");

            let contador = 1,
                min = 1,
                max = 9999;


            cambiarCantidad();

            function cambiarCantidad() {
                $(cantidad).val(contador);
            };

            $(cantidad).on("change", function(){
                let val = $(cantidad).val();
                if(val < min){
                    contador = min;
                }else if(val > max){
                    contador = max;
                }else if(isNaN(val)){
                    contador = min;
                }else{
                    contador = val; 
                }
            
                $(cantidad).val(contador);
            });

            $(mas).on('click touch', function() {
                if(contador < max){
                    contador++;
                    cambiarCantidad();
                }
            });

            $(menos).on('click touch', function() {
                if(contador > min){
                    contador--;
                    cambiarCantidad();
                }
            });
            
        });
	});
</script>