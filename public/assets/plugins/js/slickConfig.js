$(document).ready(function() {
    $(".slickSlider .container .slider").slick({
      arrows:true,
      dots: false,
      prevArrow: $('.prev'),
      nextArrow: $('.next'),
      responsive:[
        {
          breakpoint:650,
          settings:{
            dots: true
          }
        }
      ]
    });
  });