$(document).ready(function() {
    $(".slider-m1").slick({
      arrows:true,
      dots: false,
      prevArrow: $('.prev1'),
      nextArrow: $('.next1'),
      responsive:[
        {
          breakpoint:650,
          settings:{
            arrows:false,
            dots: true           
          }
        }
      ]
    });
  });