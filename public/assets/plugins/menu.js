$(document).ready(function(){
    let $menu = $("#menu").mmenu({
        extensions 	: [ "position-bottom", "fullscreen",  "border-full"],
        navbar: { add: false },
        // navbars		: [
        //     {
        //         content : [ "title", "prev"]
        //     }
        // ],
        // searchfield: {
        //     panel: true,
        //     noResults: "Sin resultados",
        //     placeholder: "Buscar en Menu",
        //     clear: true
        // },
        onClick: {
            preventDefault: false,
        },
        pageScroll: {
            scroll: true,
            update: true
        },
        iconbar: {
            "use": true,
            "bottom": [
                '<a href="https://www.instagram.com/chillside.mx"><img src="assets/img/SVG/Instagram.svg" alt="icono-instagram"></a>',
                '<a class="icono" href="https://www.facebook.com/chillside.mx"><img src="assets/img/SVG/Facebook.svg" alt="icono-facebook"></a>'
            ]
         }
    }, {
        offCanvas: {
            page: {
                selector: "#my-page"
            }
        }
    });    
    $("#menu").toggle();    
});