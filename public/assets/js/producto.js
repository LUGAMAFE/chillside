$(document).on("ready", function(){
    function scanScrolls() {
        $('.scrollbar-inner').scrollbar();
    }
    scanScrolls();

    let uploader = new qq.FineUploader({
        element: document.getElementById("file-drop-area"),
        //debug: true,
        autoUpload: false,
        request: {
            endpoint: siteURL + "subirImagen",
        },
        deleteFile: {
            enabled: true,
            endpoint: siteURL + "subirImagen",
        },
        chunking: {
            enabled: true,
            concurrent: {
                enabled: true
            },
            success: {
                endpoint: siteURL + "subirImagen?done",
            }
        },
        resume: {
            enabled: true
        },
        scaling: {
            hideScaled: true,
            //includeExif: true,
            sizes: [
                {name: "small", maxSize: 100},
                {name: "medium", maxSize: 300},
                {name: "large", maxSize: 1200}
            ]
        },
        validation: {
            allowedExtensions: ['jpeg', 'jpg', 'gif', 'png'],
            sizeLimit: 1 * 1024 * 1024 * 8,  
        },
        onComplete: function(id, fileName, responseJSON){
            if( responseJSON.success ){
                //do something 
                $.log(responseJSON.filename, responseJSON.filesize);
            }
        },
        callbacks: {
            onError: function(id, name, errorReason, xhrOrXdr) {
                if(xhrOrXdr){
                    let modal = new jBox('Modal', {
                        // width: 300,
                        // height: 100,
                        showCountdown: true,
                        autoClose: 10000,
                        delayOnHover: true,
                        title: 'Error!',
                        content: qq.format("Error en archivo numero {} - {}. </br>Razon: {}", id, name, errorReason), 
                        onClose: function() {
                            setTimeout(() => {
                                modal.destroy();
                            }, 500);
                        }
                    }).open();
                }
            }
        },
        text: {
            defaultResponseError: 'Un error desconocido a sucedido.'
        },
        messages: {
            typeError: 'El archivo {file} Tiene una extension invalida. </br>Extensiones Valida(s): {extensions}.',
            sizeError: 'El archivo {file} es demasiado grande. </br> El tamaño maximo por imagen es de {sizeLimit}.',
        },
        showMessage: function(message) {
            let modal = new jBox('Modal', {
                // width: 300,
                // height: 100,
                showCountdown: true,
                autoClose: 10000,
                delayOnHover: true,
                title: 'Error!',
                content: message, 
                onClose: function() {
                    setTimeout(() => {
                        modal.destroy();
                    }, 500);
                }
            }).open();
        }
    });

    let uploaderUsos = new qq.FineUploader({
        element: document.getElementById("usos-drop-area"),
        //debug: true,
        autoUpload: false,
        request: {
            endpoint: siteURL + "subirImagen",
        },
        deleteFile: {
            enabled: true,
            endpoint: siteURL + "subirImagen",
        },
        chunking: {
            enabled: true,
            concurrent: {
                enabled: true
            },
            success: {
                endpoint: siteURL + "subirImagen?done",
            }
        },
        resume: {
            enabled: true
        },
        scaling: {
            hideScaled: true,
            //includeExif: true,
            sizes: [
                {name: "small", maxSize: 100},
                {name: "medium", maxSize: 300},
                {name: "large", maxSize: 1200}
            ]
        },
        validation: {
            allowedExtensions: ['jpeg', 'jpg', 'gif', 'png'],
            sizeLimit: 1 * 1024 * 1024 * 8,  
        },
        onComplete: function(id, fileName, responseJSON){
            if( responseJSON.success ){
                //do something 
                $.log(responseJSON.filename, responseJSON.filesize);
            }
        },
        callbacks: {
            onError: function(id, name, errorReason, xhrOrXdr) {
                if(xhrOrXdr){
                    let modal = new jBox('Modal', {
                        // width: 300,
                        // height: 100,
                        showCountdown: true,
                        autoClose: 10000,
                        delayOnHover: true,
                        title: 'Error!',
                        content: qq.format("Error en archivo numero {} - {}. </br>Razon: {}", id, name, errorReason), 
                        onClose: function() {
                            setTimeout(() => {
                                modal.destroy();
                            }, 500);
                        }
                    }).open();
                }
            }
        },
        text: {
            defaultResponseError: 'Un error desconocido a sucedido.'
        },
        messages: {
            typeError: 'El archivo {file} Tiene una extension invalida. </br>Extensiones Valida(s): {extensions}.',
            sizeError: 'El archivo {file} es demasiado grande. </br> El tamaño maximo por imagen es de {sizeLimit}.',
        },
        showMessage: function(message) {
            let modal = new jBox('Modal', {
                // width: 300,
                // height: 100,
                showCountdown: true,
                autoClose: 10000,
                delayOnHover: true,
                title: 'Error!',
                content: message, 
                onClose: function() {
                    setTimeout(() => {
                        modal.destroy();
                    }, 500);
                }
            }).open();
        }
    });

    $(".guardar").on("click", function(){
        $("#btn-guardar").trigger("click");
    });

    $(".cancelar").on("click", function(){
        $("#btn-cancelar").trigger("click");
    });

    $("#btn-guardar").on("click", function(){
        uploader.uploadStoredFiles();
    });

    $("#btn-cancelar").on("click", function(){
        window.history.back();
    });
    
});