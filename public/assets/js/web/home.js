$(".slickMarcas").not('.slick-initialized').slick({
    prevArrow: '<button type="button" class="slick-prev "><img src="assets/img/iconos/left-white.svg" alt=""></button>',
nextArrow: '<button type="button" class="slick-next "><img src="assets/img/iconos/right-white.svg" alt=""></button>',
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: false,
    autoplaySpeed: 6000,
    arrows : false,
    lazyLoad: 'ondemand',
    responsive: [
        {
          breakpoint: 1600,
          settings: {
            settings: {
              slidesToShow: 3,
              slidesToScroll: 1,
              infinite: true,
              autoplay: true,
              autoplaySpeed: 4000,
              dots: false,
            }
          }
        },
        {
          breakpoint: 1200,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            infinite: true,
            autoplay: true,
            autoplaySpeed: 4000,
            dots: true,
          }
        },
        {
        breakpoint: 550,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          autoplay: true,
          autoplaySpeed: 4000,
          dots: true,
        }
        },
    ]
});

var scroll = new SmoothScroll('a[href*="#"]', {
  // offset: 60,
  easing: 'easeInOutCubic',
  header: "#master-header",
  speed: 800,
});

var spy = new Gumshoe('#master-header .navbar-navigator a', {
  offset: 150,
});

var spy = new Gumshoe('#menu ul a', {
  offset: 150,
});

AOS.init({
  duration: 1000,
});


/*$(document).ready(function (){
    if(mostrarNotificacion=='true') {
        new jBox('Notice', {
            content: 'Gracias, Nos pondremos en contacto con usted pronto', 
            color: 'green',
            showCountdown: true,
            autoClose: 5000,
            delayOnHover: true,
            attributes: {y: 'bottom'}}).open();
    }
});*/