function sendPostJsonAjax(url, datos, success, error, before) {
    if(typeof error === 'undefined'){
        error = function (e) {
            console.log(e)
        };
    }
    if(typeof before === 'undefined'){
        before = function () {
        };
    }

    if(typeof success === 'undefined'){
        success = function (respuesta) {
            if (respuesta.error) {
                alert("Error");
                errorReload();
            }
        }
    }

    $.ajax({
        type: "POST",
        url: url,
        data: datos,
        dataType: "json",
        beforeSend: before,
        success: success,
        error: error
    });
}

function errorReload(){
    alert("Ha Ocurrido un error de logica en el programa. La pagina se recargara para proteger la integridad de los datos del sistema.");
    location.reload();
}

$(document).ready(function() {

    let table = $('#table_id').DataTable({ 
        select: true, 
        colReorder: {
            enable: true,
            fixedColumnsRight: 1
        },
        "columnDefs": [
            {"orderable": false, "targets": 5},
            { "width": "30%", "targets": 1 }
        ],
        "language": {
            "lengthMenu":       "Mostrar _MENU_ Registros por pagina.",
            "zeroRecords":      "Ningun registro encontrado - Lo sentimos!",
            "info":             "Mostrando Pagina _PAGE_ de _PAGES_",
            "infoEmpty":        "No hay registros para mostrar.",
            "infoFiltered":     "(Filtrado de _MAX_ registros totales)",
            "loadingRecords":   "Cargando...",
            "processing":       "Procesando...",
            "search":           "Buscar:",
            "paginate": {
                "first":        "First",
                "last":         "Last",
                "next":         "Siguiente",
                "previous":     "Anterior"
            },
            "select": {
                "rows": {
                    _: "   %d Filas seleccionadas",
                    0: "",
                    1: "   Una fila seleccioanda"
                }
            }
        },
        "dom": '<"table-up-options"lfp><"table-wrapper"t><"table-down-options"ip>'
    });
    
    $.switcher('.check-mostrar');

    table.on( 'draw', function () {
        $.switcher('.check-mostrar');
    });

    table.on( 'responsive-resize', function ( e, datatable, columns ) {
        $.switcher('.check-mostrar');
    });

    table.on( 'responsive-display', function ( e, datatable, row, showHide, update ) {
        $.switcher('.check-mostrar');
    });

    //table.page.len( 1 ).draw();
    
    $(document).on("change", "tr[role='row'] .check-mostrar", function(){
        let $checkbox = $(this),
        $parentTR = $checkbox.parent().parent();
        id = $checkbox.parent().parent().find(".id-producto").text().trim(),
        activado = $checkbox.prop('checked');


        table.row( $parentTR ).invalidate();


        let url = siteURL + "actualizarVisibilidadProducto";
        let datos = {
            idProducto: id, 
            estado:activado
        };

        let success = function(respuesta){
            if(respuesta.error === true){
                errorReload();
            }
        }

        sendPostJsonAjax(url, datos, success, errorReload);
    });
});
