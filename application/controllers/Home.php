<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller {

    protected $class      = '';
    protected $folder     = '/site/';
    protected $folder_set = '/site/partials/';

    public function __construct() {
        parent::__construct();
        $this->class = strtolower(get_class());
    }

    public function index() {
        $datos = [];
        $this->template->write('title', "Home");

        $this->template->asset_js('smooth-scroll/gumshoe.min.js');
        $this->template->asset_js('smooth-scroll/smooth-scroll.min.js');
        $this->template->asset_js('slick-1.8.1/slick/slick.min.js');

        $this->template->asset_js('aos/dist/aos.js');
        $this->template->asset_css('aos/dist/aos.css');
        
        $this->template->asset_js("../js/web/home.js");
        $this->template->write_view('navbar', $this->folder_set . 'navbar', $datos);
        $this->template->write_view('contacto', $this->folder_set . 'contacto', $datos);
        $this->template->write_view('footer', $this->folder_set . 'footer', $datos);
        $this->template->write_view('content', $this->folder . 'index', $datos);
        $this->template->render();
    }

    public function enviarCorreoContacto() {
		$datos = [];
		//variables para los correos
		$motivoCorreo = "Contacto de Chillside";
		$correoEmisor = $this->input->post('correo');
		$nombreEmisor = $this->input->post('nombre');
		$claveSG = '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';

		$datosCorreo['mensaje'] = $this->input->post('cuentanos');
        $datosCorreo['nombre'] = $this->input->post('nombre');
        $datosCorreo['telefono'] = $this->input->post('telefono');

		$contenido = $this->load->view($this->folder.'correoContacto', $datosCorreo, TRUE);
		
		$receptor = [];
		$receptor['correo'] = "melissa@chillside.mx";
		$receptor['nombre'] = "Contacto Chillside";

		$this->load->library('correos',array($correoEmisor,$nombreEmisor,$contenido, $claveSG));
        $this->correos->correoSimple($motivoCorreo,array($receptor));

        $this->template->write('title', "Gracias");

        $this->template->asset_js('smooth-scroll/gumshoe.min.js');
        $this->template->asset_js('smooth-scroll/smooth-scroll.min.js');
        $this->template->asset_js('slick-1.8.1/slick/slick.min.js');

        $this->template->asset_js('aos/dist/aos.js');
        $this->template->asset_css('aos/dist/aos.css');

        $this->template->write_view('navbar', $this->folder_set . 'navbar', $datos);
        $this->template->write_view('content', $this->folder . 'thankUpage', $datos);
        $this->template->write_view('footer', $this->folder_set . 'footer', $datos);
        $this->template->render();
	}
}
