<?= doctype('html5'); ?>
<html class="no-js" lang="es">

<head <?php if(isset($prefix)) { echo $prefix; } ?>>

	<?= meta('charset', 'utf-8'); ?>
	<?= meta('viewport', 'width=device-width, initial-scale=1.0, user-scalable=no'); ?>
	<?= meta('x-ua-compatible', 'ie=edge', 'equiv'); ?>
	<meta name="description" content="Chillside">
	<meta name="author" content="">
	<base href="<?= base_url();?>">
	<title><?= $this->config->item('name_site');?> - <?= $title ?></title>
	<meta property="og:url" content="<?= current_url() ?>">
	<meta property="og:title" content="<?= $this->config->item('name_site');?> - <?= $title ?>">
	<meta property="og:description" content="<?= isset($descriptionOg) ?  $descriptionOg : "¡Hola! Somos Chillside, una agencia de marketing y programación especializada en publicidad estratégica. Estamos convencidos de que no descansáras hasta llegar a la cima, déjanos ayudarte a lograrlo." ;?>">
	<meta property="og:type" content="website">
	<meta property="og:image" content="<?= isset($imageOg) ?  $imageOg : site_url("assets/img/logo-facebook.png") ;?>">

	<?= link_tag(array('href' => 'assets/img/icon.png', 'rel' => 'shortcut icon')); ?>

	<link rel="preload" href="assets/plugins/mmenu/mburger.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
	<noscript><link rel="stylesheet" href="assets/plugins/mmenu/mburger.css"></noscript>
	<link rel="preload" href="assets/plugins/mmenu/mmenu.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
	<noscript><link rel="stylesheet" href="assets/plugins/mmenu/mmenu.css"></noscript>
	<link rel="stylesheet" href="assets/css/site.css?v=18">
	<?= $_styles; ?>
	<?= $metadatos; ?>
</head>

<body <?= $controller_angular ?>>
		<div id="my-page">
			<?= $navbar; ?>	
			<?= $content; ?>
			<?= $contacto; ?>
			<?= $footer; ?>
		</div>

	<script src="assets/plugins/js/jquery-3.4.1.min.js" type="text/javascript" charset="utf-8"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript" charset="utf-8"></script>
	<script src="assets/plugins/mmenu/mmenu.js" defer></script>
	<script src="assets/plugins/menu.js" defer></script>
	<script src="assets/plugins/lazysizes.min.js" async></script>
	<?= $_scripts; ?>
	<?php if(ENVIRONMENT == 'development'): ?>
		<script id="__bs_script__">
			document.write("<script async src='http://HOST:3000/browser-sync/browser-sync-client.js?v=2.26.5'><\/script>".replace("HOST", location.hostname));
		</script>
	<?php endif; ?>

	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDKF75u1uI_LgNOqJTpIwvM3bnkimIEqoQ&callback=initialize" async></script>
	<script>
		function initialize() {
			var styleArray = 
		[{
			stylers: [
				{ hue: "#F5E295" },
				{ saturation: "-20" },
				{ visibility: "simplified" }
			]
		}];

			var myLatlng = new google.maps.LatLng(21.032518, -89.601755);
			//var imagePath = '<?= site_url('assets/img/iconos/push-pin.svg');?>'
			var mapOptions = {
				zoom: 17,
				scrollwheel: false,
				center: myLatlng,
				styles: styleArray,
				mapTypeId: google.maps.MapTypeId.ROADMAP
			}
			var map = new google.maps.Map(document.getElementById('mapa'), mapOptions);
			var contentString = 'CULTUR';
			var infowindow = new google.maps.InfoWindow({
				content: contentString,
				maxWidth: 500
			});
			var marker = new google.maps.Marker({
				position: myLatlng,
				map: map,
				//icon: imagePath,
				title: 'CULTUR'
			});
			google.maps.event.addListener(marker, 'click', function() {
				infowindow.open(map,marker);
			});
			google.maps.event.addDomListener(window, "resize", function() {
				var center = map.getCenter();
				google.maps.event.trigger(map, "resize");
				map.setCenter(center);
			});
		}

		google.maps.event.addDomListener(window, 'load', initialize);
	</script>
</body>

</html>