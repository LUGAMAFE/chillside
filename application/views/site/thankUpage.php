<section id="thankU" class="section section-padding-x">
    <img src="assets/img/SVG/chillside-logo-color.svg" alt="logo chillside color">
    <div class="texto">
        <h1 class="head-gracias"><b>Gracias por tu mensaje.</b></h1>
        <p>Nos pondremos en contacto contigo en breve.</p>
    </div>
</section>
    