<header id="master-header">
  <nav>
    <a class="navbar-imagen" href="<?=site_url();?>">
      <img src="assets/img/SVG/responsivo.svg" width="160" height="50" alt="" />
    </a>

    <div class="burger">
				<a class="mburger" href="#menu">
					<b></b>
					<b></b>
					<b></b>
				</a>
			</div>

    <div class="menu-colapsable">
      <ul class="navbar-navigator">
        <li class="nav-item">
          <a class="nav-link" data-scroll href="#nosotros">Nosotros</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-scroll href="#marketing">Marketing</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-scroll href="#programacion">Programación</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-scroll href="#diseno">Diseño</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-scroll href="#clientes">Proyectos</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-scroll href="#contacto">Contacto</a>
        </li>
        <li class="nav-item iconos">
          <a class="icono" href="https://www.instagram.com/chillside.mx">
            <img src="assets/img/SVG/Instagram-white.svg" alt="icono-instagram">
          </a>
          <a class="icono" href="https://www.facebook.com/chillside.mx">
            <img src="assets/img/SVG/Facebook-white.svg" alt="icono-facebook">
          </a>
        </li>
      </ul>
    </div>
  </nav>
</header>

<nav id="menu">
    <ul>
      <li class="nav-item">
          <a class="nav-link" data-scroll href="#nosotros">Nosotros</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-scroll href="#marketing">Marketing</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-scroll href="#programacion">Programación</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-scroll href="#diseno">Diseño</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-scroll href="#clientes">Proyectos</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" data-scroll href="#contacto">Contacto</a>
        </li>
    </ul>
</nav>