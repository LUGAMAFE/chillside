<section id="contacto" class="contacto section section-padding">
	<div class="full-container">
		<div id="mapa"></div>

		<div class="contacto-info">
			<div class="container">
				<div data-aos="zoom-in" data-aos-anchor-placement="bottom-bottom" data-aos-duration="1500" class="logo">
					<img src="assets/img/SVG/chillside-logo-color.svg" alt="logo chillside color">
				</div>
				<div class="contacto-title">
					<img src="assets/img/SVG/contacto-title.svg" alt="contacto title">
				</div>
				<div class="info">
				Calle 28 #302 por 21 y 23 Dep 2 Colonia Montebello, Mérida, Yucatán. <br>
				<b>Tel</b> <a href="tel:9991518189"><b>9991518189</b></a> <br>
				<a href="mailto:melissa@chillside.mx">melissa@chillside.mx</a>
				</div>
			</div>
		</div>
	</div>
</section>