<footer>
    <div class="wave">
        <img src="assets/img/SVG/yellow-wave.svg">
    </div>

    <div class="info-footer">
        <ul class="nav redes">
            <li class="nav-item iconos">
                <a class="icono" href="https://www.instagram.com/chillside.mx">
                    <img src="assets/img/SVG/Instagram-white.svg" alt="icono-instagram">
                </a>
                <a class="icono" href="https://www.facebook.com/chillside.mx">
                    <img src="assets/img/SVG/Facebook-white.svg" alt="icono-facebook">
                </a>
            </li>

            <li class="nav-item d-flex align-items-center">
                <a class="nav-link" href="#">©2020 Chillside</a>
            </li>
        </ul>
    </div>
</footer>