<section id="nosotros" class="section section-padding-x">
	<div class="double-column-container">
        <div data-aos="fade-right" class="imagen-section">
            <div class="imagen-container">
                <img src="assets/img/SVG/eslogan.svg" alt="eslogan chillside">
                <div class="texto-chilside">
                    <div class="texto">
                        ¡Hola! Somos <b>Chillside</b>, una agencia de marketing y programación especializada en publicidad estratégica. Estamos convencidos de que no descansáras hasta llegar a la cima, déjanos ayudarte a lograrlo.
                    </div>
                </div>
            </div>
        </div>

        <div data-aos="fade-left" class="formulario-contacto">
            <form action="<?=site_url('gracias')?>" method="POST">
                <textarea name="cuentanos" cols="30" rows="4" placeholder="¡Cuéntanos de tu proyecto!" required></textarea>
                <input type="text" name="nombre" placeholder="Nombre" required>
                <input type="tel" name="telefono" placeholder="Teléfono" required>
                <input type="email" name="correo" placeholder="Correo" required>
                <input type="submit" value="Enviar">
            </form>
        </div>
	</div>
</section>

<section id="marketing" class="section section-padding-x">
	<div class="double-column-container">
        <div class="info-categoria">
            <div data-aos="fade-up" class="img-title">
                <img src="assets/img/SVG/marketing-title.svg" alt="marketing titulo">
            </div>

            <div data-aos="flip-left" data-aos-anchor-placement="center-bottom" data-aos-duration="2000" class="info">
                <p>Como agencia de publicidad desarrollamos estrategias de marketing digital inspiradas en inbound marketing.</p>

                <span>Algunas herramientas que utilizamos:</span>
                <ul>
                    <li>Marketing de contenidos</li>
                    <li>SEO</li>
                    <li>Campañas pagadas </li>
                    <li>Redes sociales</li>
                </ul>
            </div>
        </div>

        <div data-aos="zoom-in-left" data-aos-anchor-placement="center-bottom" data-aos-duration="1500" class="imagen-section">
            <div class="imagen-container">
                <img src="assets/img/SVG/marketing-img.svg" alt="mano tocando celular">
            </div>
        </div>
	</div>
</section>


<section id="programacion" class="section section-padding-x">
    <div class="mancha">
        <img src="assets/img/SVG/manchas.svg" alt="manchas">
    </div>
	<div class="double-column-container">
        <div data-aos="zoom-in-right" data-aos-anchor-placement="center-bottom" data-aos-duration="1500" class="imagen-section">
            <div class="imagen-container">
                <img src="assets/img/SVG/programacion-img.svg" alt="mano tocando laptop">
            </div>
        </div>

        <div class="info-categoria">
            <div data-aos="fade-up" class="img-title">
                <img src="assets/img/SVG/programacion-title.svg" alt="programacion titulo">
            </div>

            <div data-aos="flip-right" data-aos-anchor-placement="center-bottom" data-aos-duration="2000" class="info">
                <p>Más que un sitio web nos encargamos de crear herramientas que se alineen con el funcionamiento de tu negocio mediante programación pura y escalable.</p>

                <span>Servicios:</span>
                <ul>
                    <li>Landing page</li>
                    <li>E-commerce</li>
                    <li>Sitos web informativos </li>
                    <li>Desarrollo de Apps</li>
                </ul>
            </div>
        </div>
	</div>
</section>

<section id="diseno" class="section section-padding-x">
    <div class="mancha2">
        <img src="assets/img/SVG/manchas2.svg" alt="manchas">
    </div>
	<div class="double-column-container">
        <div class="info-categoria">
            <div data-aos="fade-up" class="img-title">
                <img src="assets/img/SVG/diseno-title.svg" alt="diseño titulo">
            </div>

            <div data-aos="flip-up" data-aos-anchor-placement="center-bottom" data-aos-duration="2000" class="info">
                <p>Plasmamos la identidad de tu marca y lo que necesitas que proyecte en:</p>

                <ul>
                    <li>Diseño gráfico</li>
                    <li>Diseño web</li>
                    <li>Animación </li>
                    <li>Ilustración </li>
                </ul>
            </div>
        </div>

        <div data-aos="zoom-in-left" data-aos-anchor-placement="center-bottom" data-aos-duration="1500" class="imagen-section">
            <div class="imagen-container">
                <img src="assets/img/SVG/diseno-img.svg" alt="mano tocando tablet">
            </div>
        </div>
	</div>
</section>

<section data-aos="fade-up-right" data-aos-anchor-placement="top-center" id="clientes" class="section section-padding-x">
    <div class="chillers-title">
        <img src="assets/img/SVG/chillers-header.svg" alt="chillers">
    </div>
    <div class="chillers-slider">
        <div class="slickContainer slickMarcas">
        <div class="box">
				<div class="body">
					<a href="#">
						<div class="img">
							<img src="<?= site_url('assets/img/SVG/911-alarmas.svg');?>" alt="">
						</div>
					</a>
				</div>
            </div>
            <div class="box">
				<div class="body">
					<a href="#">
						<div class="img">
							<img src="<?= site_url('assets/img/SVG/logo-cultur.svg');?>" alt="">
						</div>
					</a>
				</div>
			</div>
			<div class="box">
				<div class="body">
				<a href="#">
						<div class="img">
							<img src="<?= site_url('assets/img/SVG/disegnomia.svg');?>" alt="">
						</div>
					</a>
				</div>
			</div>
			<div class="box">
				<div class="body">
				<a href="#">
						<div class="img">
							<img src="<?= site_url('assets/img/mayan-life.png');?>" alt="">
						</div>
					</a>
				</div>
			</div>
		</div>
    </div>
</section>


<section id="relajate" class="section section-padding-x">
	<div class="double-column-container">
        <div class="imagen-section">
            <div data-aos="fade-right" data-aos-anchor-placement="center-bottom" data-aos-duration="1400" class="imagen-container">
                <img src="assets/img/SVG/relajate.svg" alt="eslogan chillside">
            </div>
        </div>

        <div data-aos="fade-left" data-aos-anchor-placement="center-bottom" data-aos-duration="1400" class="formulario-contacto">
            <form action="<?=site_url('gracias')?>" method="POST">
                <textarea name="cuentanos" cols="30" rows="4" placeholder="¡Cuéntanos de tu proyecto!" required></textarea>
                <input type="text" name="nombre" placeholder="Nombre" required>
                <input type="tel" name="telefono" placeholder="Teléfono" required>
                <input type="email" name="correo" placeholder="Correo" required>
                <input type="submit" value="Enviar">
            </form>
        </div>
	</div>
</section>
