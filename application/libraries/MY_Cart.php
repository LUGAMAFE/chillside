<?php
defined('BASEPATH') OR exit('No direct script access allowed');


/*use FedEx\ShipService;
use FedEx\ShipService\ComplexType;
use FedEx\ShipService\SimpleType;*/

class MY_Cart extends CI_Cart {

	var $product_name_rules = '\d\D';

	public function modificar_carrito_cupon()
	{
		$ci =& get_instance();
		$carrito = $ci->cart->contents();
		$descuento = $ci->session->userdata('cupon');
		$cupon = $ci->session->userdata('descuento_cupon');
		foreach($carrito as $row) {
			$opciones = $row['options'];
			$diferencia = $opciones['precio'] * ($descuento * 0.01);
			$costo_producto = descuento_producto($descuento, $opciones['precio']);
			$opciones['descuento_producto'] = $descuento;
			$opciones['diferencia_descuento'] = $diferencia;
			$update_producto = array(
		        'rowid'  => $row['rowid'],
		        'price'  => $costo_producto['precio'],
		        'coupon' => $cupon,
		        'options' => $opciones
			);
			$ci->cart->update($update_producto);
		}
	}

	public function get_total()
	{
		$subtotal = 0;
		$ci =& get_instance();
		$carrito = $ci->cart->contents();
		foreach($carrito as $row) {
			$subtotal = $subtotal + $row['subtotal'];
		}

		$total =$subtotal;
		$subtotal = $subtotal * .84;
		$iva = $total * .16;

		return array('total' => $total, 'subtotal' => $subtotal, 'iva' => $iva);
	}

	public function get_dias_envio($items = '')
	{
		$dias = array();
		$dia = 0;
		if(!empty($items) && is_array($items)) {
			foreach($items as $row) {
				array_push($dias, $row['options']->tiempo_envio);
			}
		}
		arsort($dias);
		$count = 1;
		foreach($dias as $row) {
			if($count == 1) {
				$dia = $row;
			}
			$count++;
		}
		return $dia;
	}

	public function peso_total($items = '')
	{
		$peso = 0;
		if(!empty($items) && is_array($items)) {
			foreach($items as $row) {
				//print_m($row);
				//echo $row['options']['peso_total'];
				$peso += $row['options']->peso_total;
			}
			$peso = ($peso > 1)? $peso : 1;
		}
		return (!empty($peso)) ? $peso : 1;
	}

	public function iva()
	{
		return $this->format_number($this->total() * 0.84);
	}

	public function obtener_subtotal()
	{
		//$ci =& get_instance();
		return $this->format_number($this->total());
	}

	public function obtener_total($envio = '')
	{
		$ci =& get_instance();
		if($ci->session->userdata('puntos_canjear')) {
			return ($this->obtener_subtotal() - $ci->session->userdata('puntos_canjear')) + $envio;
		} else {
			return $this->obtener_subtotal() + $envio;
		}
		
	}

	/*public function cantidad_producto($data = '')
	{
		$ci =& get_instance();
		$ci->load->model('productos/producto');
		$producto = $ci->producto;

		$cantidad = $producto->select('cantidad_producto')->where('id_producto', $data['id'])->get()->row();
		return $cantidad->cantidad_producto;
	}*/

	public function obtener_costo_envio() 
	{
		$ci =& get_instance();
		if($this->obtener_subtotal() >= $ci->configuracion->max_envio) { 
			return 0;
		} else {
			return $ci->configuracion->costo_envio;
		}
	}

	public function guardar_carrito_session()
	{
		$ci =& get_instance();
		$ci->load->model('clientes/cliente');
		//print_m();
		if($ci->session->userdata('id')) {
			$id = $ci->session->userdata('id');
			/* validar si existe sesion de carrito */
			if($ci->cart->total_items() != 0) {
				$cliente = $ci->cliente->get($id);
				if($cliente->num_rows()) {
					$cliente = $cliente->row();
					if($ci->cart->contents()) {
						$carrito = base64_encode(json_encode($ci->cart->contents()));
					} else {
						$carrito = '';
					}
					$update = array(
						'carrito_cliente' => $carrito,
						'update_carrito' => date('Y-m-d')
					);
					//print_m($update);
					$ci->cliente->update($update, $id);
				}
			}
		}
	}

	public function construir_carrito()
	{
		/* comprabar si tenemos session iniciada */
		/* validar cupon si esta vigente y ponerselo al producto / OJO */
		$ci =& get_instance();
		//print_m($ci->cart->contents());
		$ci->load->model('clientes/cliente');
		//print_m();
		if($ci->session->userdata('id')) {
			//echo 'entro';
			$id = $ci->session->userdata('id');
			/* validar si existe sesion de carrito */
			if($ci->cart->total_items() == 0) {
				$cliente = $ci->cliente->get($id);
				if($cliente->num_rows()) {
					//echo 'entro';
					$cliente = $cliente->row();
					if(!empty($cliente->carrito_cliente)) {
						//echo 'entro';
						//echo $cliente->carrito_cliente;
						$carrito = json_decode(base64_decode($cliente->carrito_cliente));
						//print_m($carrito);
						foreach($carrito as $row){
							$producto = array(
								'id'      => $row->id,
						        'qty'     => $row->qty,
						        'price'   => $row->price,
						        'name'    => $row->name,
						        'options' => (array) $row->options
							);
							/*
								Validar cantidad del producto
							*/
							$ci->cart->insert($producto);
						}
					}
				}
			}
		}
	}
}
