<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
    require '../vendor/autoload.php';
    class Correos {
        
        
        private $sendKey;
        private $correoEmisor;
        private $nombreEmisor;
        private $vistaCorreo;
        //<h1> nombre </h1>
        private $claveMientras = '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';

        public function __construct($array = []) {
            $this->correoEmisor = $array[0];
            $this->nombreEmisor = $array[1];
            $this->vistaCorreo = $array[2];
            $this->sendKey = $array[3];
        }

       
           
        public function enviarCorreoAd($motivoCorreo, $receptores, $nombreArchivo,$tipoArchivo,$archivoAdjunto){
             $faseUno = $this->crearCorreo($motivoCorreo, $receptores);
             $faseDos = $this->correoAdjunto($faseUno, $nombreArchivo,$tipoArchivo,$archivoAdjunto);
             $this->enviarCorreo($faseDos);
        }
        //array
        public function correoSimple($motivoCorreo, $receptores){
            $faseUno = $this->crearCorreo($motivoCorreo ,$receptores);
            $this->enviarCorreo($faseUno);
        }


        private function crearCorreo($motivoCorreo, $receptores){
            $email = new \SendGrid\Mail\Mail();
            $email->setFrom($this->correoEmisor,$this->nombreEmisor);
            $email->setSubject($motivoCorreo);

            foreach($receptores as $receptor){
                $email->addTo($receptor['correo'] , $receptor['nombre']);
            }

            $email->addContent("text/html", $this->vistaCorreo);

            return $email;
        }

        private function enviarCorreo($email){

            $sendgrid = new \SendGrid($this->claveMientras);
            try {
                $response = $sendgrid->send($email);
            } catch (Exception $e) {
                echo 'Caught exception: '.  $e->getMessage(). "\n";
            }

        }

        //correo que puede enviar archivo adjunto
        private function correoAdjunto($email,$nombreArchivo,$tipoArchivo,$archivoAdjunto){
            $file_encoded = base64_encode($archivoAdjunto);
            $email->addAttachment(
                $file_encoded,
                $tipoArchivo,
                $nombreArchivo,
                "attachment"
            );
            return $email;      
        }
    }
	
    
?>
