<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <?= link_tag('assets/css/admin.css?v=0.5'); ?>
    <?= link_tag('assets/plugins/scrollbar/jquery.scrollbar.css'); ?>
    <?= link_tag('assets/plugins/jquerymodal/jquery.modal.min.css'); ?>
    <?= link_tag('assets/plugins/custominputfile/css/jquery.Jcrop.min.css'); ?>
    <link href="https://cdn.jsdelivr.net/gh/StephanWagner/jBox@v1.0.3/dist/jBox.all.min.css" rel="stylesheet">
    <?= link_tag('assets/plugins/custominputfile/css/custominputfile.min.css'); ?>

    <script src="<?= site_url('assets/js/modernizr.js'); ?>"></script>
    <script src="<?= site_url('assets/js/jquery.min.js')?>"></script>

    <script src="<?= site_url('assets/plugins/custominputfile/js/jquery.Jcrop.min.js')?>"></script>
    <script src="<?= site_url('assets/plugins/custominputfile/js/custominputfile.js')?>"></script>

    <script src="https://cdn.jsdelivr.net/gh/StephanWagner/jBox@v1.0.3/dist/jBox.all.min.js"></script>

    <script src="<?= site_url('assets/plugins/jquerymodal/jquery.modal.min.js'); ?>"></script>
    <script src="<?= site_url('assets/plugins/sortablejs/Sortable.js'); ?>"></script>
    <script src="<?= site_url('assets/plugins/jquery-sortablejs/jquery-sortable.js'); ?>"></script>
    <script src="<?= site_url('assets/plugins/scrollbar/jquery.scrollbar.min.js'); ?>"></script>
    <script src="<?= site_url('assets/js/categorias.js')?>"></script>
    <script>
        const siteURL = "<?= site_url("categorias");?>/";
    </script>
</head>
<body>
    <div class="contenedorCategorias">
        <div class="arriba">
            <p class="titulo">Categorias</p>
            <div class="secciones">
                <div class="listaCategorias">
                    <div class="contenedorAgregar">
                        <div id="agregarCat"class="agregar">
                            <input type="text" name="nuevaCategoria" id="nuevaCategoria" placeholder="Categoria Nueva">
                            <div class="add">+</div>
                        </div>
                    </div> 
                    <div id="contenedorCate" class="contenedorCate scrollbar-inner">
                        <?php for ($i=$categoriasDesplegadas; $i < count($categorias); $i++): $categoria = $categorias[$i]?>
                            <div class="categoria">
                                <div class="nombre">
                                    <img class="handler svg" src="<?= site_url('assets/img/iconos/move-arrows.svg');?>" alt="">
                                    <p class="nombreCategoria"><?=$categoria["nombre_categoria"]?></p>
                                    <a href="#modalEditarCategoria"><img class="options svg" src="<?= site_url('assets/img/iconos/menu-bars.svg');?>" alt=""></a>
                                </div>
                                <ul id-categoria="<?=$categoria["id-categoria"]?>" class="grupos scrollbar-inner contenedor-grupos"> 
                                    <?php foreach ($categoria["grupos"] as $grupo):
                                        if($grupo["id_grupo"] !== NULL && $grupo["nombre_grupo"] !== NULL  ):?>
                                        <li id-grupo="<?= $grupo["id_grupo"]; ?>" class="grupo"><img class="handler-grupo svg" src="<?= site_url('assets/img/iconos/six-dots.svg');?>" alt=""> <?= $grupo["nombre_grupo"]; ?> </li>
                                        <?php endif; endforeach;?>
                                </ul>
                            </div>
                        <?php endfor; ?>
                    </div>
                </div>

                <div id="despliegueCategorias" class="despliegueCategorias">
                    <?php for ($i=0; $i < $categoriasDesplegadas; $i++): $categoria = $categorias[$i]?>
                        <div class="categoria">
                            <div class="nombre">
                                <img class="handler svg" src="<?= site_url('assets/img/iconos/move-arrows.svg');?>" alt="">
                                <p class="nombreCategoria"><?=$categoria["nombre_categoria"]?></p>
                                <a href="#modalEditarCategoria"><img class="options svg" src="<?= site_url('assets/img/iconos/menu-bars.svg');?>" alt=""></a>
                            </div>
                            <ul id-categoria="<?=$categoria["id-categoria"]?>" class="grupos scrollbar-inner contenedor-grupos"> 
                                <?php foreach ($categoria["grupos"] as $grupo):
                                    if($grupo["id_grupo"] !== NULL && $grupo["nombre_grupo"] !== NULL  ):?>
                                    <li id-grupo="<?= $grupo["id_grupo"]; ?>" class="grupo"><img class="handler-grupo svg" src="<?= site_url('assets/img/iconos/six-dots.svg');?>" alt=""> <?= $grupo["nombre_grupo"]; ?> </li>
                                    <?php endif; endforeach;?>
                            </ul>
                        </div>
                    <?php endfor; ?>
                </div>
            </div>
        </div>

        <div class="abajo">
            <div class="contenedorCategoriasBasura">
                <p class="titulo">Eliminar</p>
                <div id="categoriasBasura" class="contenidoBasura">
                </div>
            </div>
            <div class="contenedorGrupos">
                <p class="titulo">Grupos Pendientes a Asignar</p>
                <ul id="listaGruposPendientes" class="grupos scrollbar-inner"> 
                    <?php foreach ($gruposPendientes as $grupo):?>
                        <li id-grupo="<?= $grupo["id_grupo"]; ?>" class="grupo"><img class="handler-grupo svg" src="<?= site_url('assets/img/iconos/six-dots.svg');?>" alt=""> <?= $grupo["nombre_grupo"]; ?> </li>
                    <?php endforeach;?>
                </ul>
            </div>
        </div>
    </div>

    <!-- Modal HTML embedded directly into document -->
    <div id="modalEditarCategoria" class="modal" style="display:none;">
        <a class="closeModalCancelar" href="#" rel="modal:close">Cancelar</a>
        <p class="titulo">Categoria: <span id-categoria="" class="categoria"></span></p>
        <div class="opcion opcionesBanner">
            <p class="textoOpcion"> <span>Opción: </span> Cambiar Banner de Categoria</p>
            <input name="input-file-1" id="input-file-1"> 
        </div>
        <div class="opcion opcionesTextoCategoria">
            <p class="textoOpcion"> <span>Opción: </span> Cambiar Texto Informativo Categoria</p>
            <label>Texto Categoria:<input type="text" placeholder="Cambiar Texto"></label>
            
        </div>
    </div>
</body>
</html>

