<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Dashboard Contenido Pagina</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?= site_url("administracion") ?>">Home</a></li>
            <li class="breadcrumb-item active">Contenido Pagina</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
      <div class="row d-flex justify-content-center justify-content-md-start">
        <div class="col-12 col-md-10 col-lg-6">
          <!-- small box -->
          <div class="small-box bg-danger">
            <div class="inner">
              <h3>Home</h3>

              <p>Admin</p>
            </div>
            <div class="icon">
              <i class="fas fa-home"></i>
            </div>
            <a href="<?= site_url("administracion/pagina/home") ?>" class="small-box-footer">Más Info <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-12 col-md-10 col-lg-6">
          <!-- small box -->
          <div class="small-box bg-danger">
            <div class="inner">
              <h3>Remates</h3>

              <p>Admin</p>
            </div>
            <div class="icon">
              <i class="fas fa-piggy-bank"></i>
            </div>
            <a href="<?= site_url("administracion/pagina/remates") ?>" class="small-box-footer">Más Info <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-12 col-md-10 col-lg-6">
          <!-- small box -->
          <div class="small-box bg-danger">
            <div class="inner">
              <h3>Bolsa de Trabajo</h3>

              <p>Admin</p>
            </div>
            <div class="icon">
              <i class="fas fa-briefcase"></i>
            </div>
            <a href="<?= site_url("administracion/pagina/bolsa-trabajo") ?>" class="small-box-footer">Más Info <i class="fas fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
      
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>

<?php echo $this->load->view('admin/utils/sweetAlerts', '', true); ?>