<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Admin Pagina Home </h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?= site_url("administracion") ?>">Home</a></li>
            <li class="breadcrumb-item"><a href="<?= site_url("administracion/pagina") ?>">Contenido Pagina</a></li>
            <li class="breadcrumb-item active">Admin Pagina Home</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
    <div class="row justify-content-center">
        <!-- left column -->
        <div class="col-md-11">
          <!-- general form elements -->
          <div class="card card-dark">
            <div class="card-header">
              <h3 class="card-title">Editar Pagina Home</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form id="form-entrada" action="<?= $action ?>" enctype="multipart/form-data" method="POST" role="form"> 
              <div class="card-body">
                <div class="row justify-content-center">
                    <div class="col-12">
                            <div class="form-group urls-container">
                                <label>Consejos del Rey Videos</label>
                                <?php if(count($youtube_urls) <= 0): ?>
                                  <div class="input-group">
                                    <input type="url" class="form-control" name="urls[]" placeholder="Url Video Youtube" required>
                                    <span class="input-group-append">
                                      <button type="button" class="btn btn-primary text-light btn-flat add_button_urls">Agregar Url</button>
                                    </span>
                                  </div>
                                <?php else: ?>  
                                    <?php for ($i=0; $i < count($youtube_urls); $i++): 
                                      $url = $youtube_urls[$i];
                                      if($i == 0):
                                    ?>
                                      <div class="input-group">
                                        <input type="url" class="form-control" name="urls[]" placeholder="Url Video Youtube" required value="<?= $url["url_video"] ?>">
                                        <span class="input-group-append">
                                          <button type="button" class="btn btn-primary text-light btn-flat add_button_urls">Agregar Url</button>
                                        </span>
                                      </div>
                                    <?php else: ?>  
                                      <div class="input-group mt-2">
                                        <input type="url" class="form-control" name="urls[]" placeholder="Url Video Youtube" required value="<?= $url["url_video"] ?>">
                                        <span class="input-group-append">
                                        <button type="button" class="btn btn-danger text-light btn-flat remove-button-url"><i class="fas fa-times"></i></button>
                                        </span>
                                      </div>
                                    <?php endif; endfor; ?>
                                <?php endif;?>
                            </div>
                    </div>
                    
                    <div class="col-12 col-md-6">

                        <div class="form-group">
                            <label>Video Barra Promociones</label>
                            <div class="upload-area video requerido" id="video-barra-promociones"></div>
                            <input type="hidden" name="video-barra-promociones-info">
                        </div>

                        <div class="form-group">
                            <label>Imagen Barra Promociones</label>
                            <div class="upload-area individual requerido" id="imagen-barra-promociones"></div>
                            <input type="hidden" name="imagen-barra-promociones-info">
                        </div>

                        <div class="form-group">
                            <label>Banner Principal</label>
                            <div class="upload-area multiple requerido" id="imagen-banner-principal"></div>
                            <input type="hidden" name="imagen-banner-principal-info">
                        </div>

                    </div>  
                    <div class="col-12 col-md-6">

                        <div class="form-group">
                            <label>Banner Secundario</label>
                            <div class="upload-area individual requerido" id="imagen-banner-secundario"></div>
                            <input type="hidden" name="imagen-banner-secundario-info">
                        </div>

                        <div class="form-group">
                            <label>Banner Terciario</label>
                            <div class="upload-area individual requerido" id="imagen-banner-terciario"></div>
                            <input type="hidden" name="imagen-banner-terciario-info">
                        </div>

                        <div class="form-group">
                            <label>Quienes Somos Imagen</label>
                            <div class="upload-area individual requerido" id="imagen-quienes-somos"></div>
                            <input type="hidden" name="imagen-quienes-somos-info">
                        </div>

                    </div>  
                </div>   
                
              </div>
              <!-- /.card-body -->
              <div class="card-footer">
                <a href="<?= site_url("administracion/pagina") ?>"  class="btn btn-danger btn-cancelar">Cancelar</a>
                <div id="submit-form" class="btn btn-info float-right">Guardar Entrada</div>
              </div>
            </form>
          </div>
          <!-- /.card -->
        </div>
        <!--/.col-->
      </div>
      <!-- /.row -->
      
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>

<?= $this->load->view('admin/utils/sweetAlerts', '', true); ?>
