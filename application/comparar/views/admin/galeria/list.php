<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Casos Exito Admin <i class="nav-icon fas fa-images"></i></h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?= site_url("administracion") ?>">Home</a></li>
            <li class="breadcrumb-item active">Galeria Admin</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
    <div class="container-fluid">
    <div class="row justify-content-center">
      <div class="col-12">
        <div class="card card-secondary">
          <div class="card-header">
            <h3 class="card-title">Tabla Casos Exito</h3>
          </div>
          <!-- /.card-header -->
          <div class="card-body">
            <div class="row justify-content-end">
                <div class="col-sm-12 col-md-6 col-lg-4 col-xl-2 mb-2 d-flex justify-content-end">
                  <a href="<?= site_url("administracion/galeria/crear-caso") ?>" class="btn btn-info btn-block btn-md text-white"><i class="fas fa-plus mr-1"></i>Crear Caso Exito</a>
                </div>
              </div>
              <div class="row">
                <div class="col-12">
                  <table id="tabla-casos" class="table table-bordered table-striped dt-responsive">
                    <thead>
                    <tr>
                      <th>Id Caso</th>
                      <th>Nombre Caso</th>
                      <th>Editar</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                      <td>Trident</td>
                      <td>Win 95+</td>
                      <td>
                        <div class="row">
                          <div class="col-6 d-flex justify-content-center">
                            <button type="button" class="btn btn-info d-flex justify-content-center align-items-center p-1 p-lg-2"><i style="padding-left: 3px;" class="fas fa-edit pr-xl-1"></i> <span class="d-none d-xl-block">Editar</span> </button>
                          </div>
                          <div class="col-6 d-flex justify-content-center">
                            <button type="button" class="btn btn-danger d-flex justify-content-center align-items-center p-1 p-lg-2"><i style="padding-left: 3px;" class="far fa-trash-alt pr-xl-1"></i> <span class="d-none d-xl-block">Eliminar</span> </button>
                          </div>
                        </div>
                      </td>
                    </tr>
                    </tbody>
                    <tfoot>
                    <tr>
                      <th>Id Caso</th>
                      <th>Nombre Caso</th>
                      <th>Editar</th>
                    </tr>
                    </tfoot>
                  </table>
                </div>
                <!-- /.col -->
              </div>
              <!-- /.row --> 
          </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
      
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>

<script>
  $(function () {
    $('#tabla-casos').DataTable({
      "paging": true,
      "lengthChange": true,
      "searching": true,
      "ordering": true,
      "info": true,
      "autoWidth": false,
    });
  });
</script>

<?php echo $this->load->view('admin/utils/sweetAlerts', '', true); ?>