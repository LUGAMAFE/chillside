<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark">Casos Exito Admin <i class="nav-icon fas fa-images"></i></h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?= site_url("administracion") ?>">Home</a></li>
            <li class="breadcrumb-item"><a href="<?= site_url("administracion/galeria") ?>">Galeria</a></li>
            <li class="breadcrumb-item active">Casos Exito Admin</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row justify-content-center">
        <!-- left column -->
        <div class="col-md-11">
          <!-- general form elements -->
          <div class="card card-dark">
            <div class="card-header">
              <h3 class="card-title">Control Caso Exito</h3>
            </div>
            <!-- /.card-header -->
            <!-- form start -->
            <form id="form-caso" role="form"> 
              <div class="card-body">
                <div class="row justify-content-center align-items-center">
                    <div class="col-s-12 col-md-6">
                        <div class="form-group">
                            <label>Titulo</label>
                            <input type="text" class="form-control" placeholder="Titulo.." required>
                        </div>
                        <div class="form-group">
                            <label>Lugar</label>
                            <input type="text" class="form-control" placeholder="Lugar.." required>
                        </div>
                        <div class="form-group">
                            <label>Capacidad Instalada</label>
                            <input type="number" class="form-control" placeholder="Capacidad.." required>
                        </div>
                        <div class="form-group">
                            <label>Producción Anual</label>
                            <input type="number" class="form-control" placeholder="Producción.." required>
                        </div>
                        <div class="form-group">
                            <label>Información</label>
                            <textarea class="form-control" rows="3" placeholder="Informacion Caso..." required></textarea>
                        </div>
                    </div>  
                    <div class="col-s-12 col-md-6">
                        <div class="form-group">
                            <label>Imagen del Caso de Exito</label>
                            <div class="upload-area requerido" id="file-drop-area-caso"></div>
                        </div>
                    </div>  
                </div>   
                
              </div>
              <!-- /.card-body -->
              <div class="card-footer">
                <button type="submit" class="btn btn-info">Guardar Caso Exito</button>
              </div>
            </form>
          </div>
          <!-- /.card -->
        </div>
        <!--/.col-->
      </div>
      <!-- /.row -->
      
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>

<?php echo $this->load->view('admin/utils/sweetAlerts', '', true); ?>