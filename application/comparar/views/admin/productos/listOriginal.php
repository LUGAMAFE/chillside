<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <?= link_tag('assets/plugins/scrollbar/jquery.scrollbar.css'); ?>
    <?= link_tag('assets/plugins/DataTables/datatables.min.css'); ?>
    <?= link_tag('assets/css/admin.css?v=0.5'); ?>
    <?= link_tag('assets/plugins/Switcher/css/switcher.css'); ?>
    
    <script src="<?= site_url('assets/js/modernizr.js'); ?>"></script>
    <script src="<?= site_url('assets/js/jquery.min.js')?>"></script>
    <script src="<?= site_url('assets/plugins/Switcher/js/jquery.switcher.js'); ?>"></script>
    <script src="<?= site_url('assets/plugins/sortablejs/Sortable.js'); ?>"></script>
    <script src="<?= site_url('assets/plugins/jquery-sortablejs/jquery-sortable.js'); ?>"></script>
    <script src="<?= site_url('assets/plugins/scrollbar/jquery.scrollbar.min.js'); ?>"></script>
    <script src="<?= site_url('assets/plugins/DataTables/datatables.min.js'); ?>"></script>
    <script>
        const siteURL = "<?= site_url("productos");?>/";
    </script>
    <script src="<?= site_url('assets/js/productos.js')?>"></script>
</head>
<body>
    <div class="contenedorProductos">
        <div class="despliegueTablaProductos">
            <p class="titulo">Productos</p>
            <div class="tablaProductos">
                <table id="table_id" class="display responsive" style="width:100%">
                    <thead>
                        <tr>
                            <th>Articulo</th>
                            <th>Nombre Articulo</th>
                            <th>Categoria</th>
                            <th>Grupo</th>
                            <th>Mostrar</th>
                            <th>Info</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $id=0; foreach ($productos as $producto):?>
                            <tr>
                                <td class="id-producto"> <?= $producto["id_mcp"];?> </td>
                                <td> <?= $producto["Descripcion1"];?> </td>
                                <td> <?= is_null($producto["nombre_categoria"]) ? "Sin Categorizar" : $producto["nombre_categoria"]; ?> </td>
                                <td> <?= $producto["nombre_grupo"];?> </td>
                                <td> <input data-check-id="<?= $id++ ?>" type="checkbox" class="check-mostrar <?=$producto["visible_prod"] === "1" ? 'datachecked' : '';?>"> <span class="mostrar"><?=$producto["visible_prod"] === "1" ? 'mostrar' : 'ocultar';?></span> </td>
                                <td> 
                                    <a href="<?php echo site_url('adminproducto/home').'/'.htmlspecialchars(trim($producto["id_mcp"]));?>"><button attr-id-prod="<?= $producto["id_mcp"];?>" class="btnEditar">Editar Producto</button></a>
                                </td>
                            </tr>
                        <?php endforeach;?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>
</html>