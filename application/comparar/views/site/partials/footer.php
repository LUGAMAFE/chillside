<?php
		date_default_timezone_set('Mexico/General');
		$date = new DateTime('NOW');
		$dateTime = $date->format('Y');
?>
<div class="row expanded formas-pago">
	<div class="row expanded pago-tienda">
		<h1 class="titulo-pago">formas de pago EN TIENDA:</h1>
		<div class="row expanded small-up-2 medium-up-4 large-up-4 lista-items">
			<div class="column column-block">
				<div class="img">
					<img src="<?php echo site_url('assets/img/fondo-empresa-home.png');?>" style="background-image: url('<?php echo site_url('assets/img/iconos/visa.svg');?>');" alt="">
				</div>
				<div class="texto">
					<p>Tarjeta de crédito</p>
				</div>
			</div>
			<div class="column column-block">
				<div class="img">
					<img src="<?php echo site_url('assets/img/fondo-empresa-home.png');?>" style="background-image: url('<?php echo site_url('assets/img/iconos/master.svg');?>');" alt="">
				</div>
				<div class="texto">
					<p>Tarjeta de crédito</p>
				</div>
			</div>
			<div class="column column-block">
				<div class="img">
					<img src="<?php echo site_url('assets/img/fondo-empresa-home.png');?>" style="background-image: url('<?php echo site_url('assets/img/iconos/american-express.svg');?>');" alt="">
				</div>
				<div class="texto">
					<p>American Express</p>
				</div>
			</div>
			<div class="column column-block">
				<div class="img">
					<img src="<?php echo site_url('assets/img/fondo-empresa-home.png');?>" style="background-image: url('<?php echo site_url('assets/img/iconos/efectivo.svg');?>');" alt="">
				</div>
				<div class="texto">
					<p>Efectivo</p>
				</div>
			</div>
		</div>
	</div>
	<div class="row expanded pago-credito">
		<h1 class="titulo-pago">ACEPTAMOS CRÉDITO:</h1>
		<div class="row expanded small-up-2 medium-up-3 large-up-6 lista-items">
			<div class="column column-block">
				<div class="img">
					<img src="<?php echo site_url('assets/img/fondo-empresa-home.png');?>" style="background-image: url('<?php echo site_url('assets/img/iconos/mejoravit.png');?>');" alt="">
				</div>
				<div class="texto">
					<p>Mejoravit</p>
				</div>
			</div>
			<div class="column column-block">
				<div class="img">
					<img src="<?php echo site_url('assets/img/fondo-empresa-home.png');?>" style="background-image: url('<?php echo site_url('assets/img/iconos/fonacot.png');?>');" alt="">
				</div>
				<div class="texto">
					<p>Crédito FONACOT</p>
				</div>
			</div>
			<div class="column column-block">
				<div class="img">
					<img src="<?php echo site_url('assets/img/fondo-empresa-home.png');?>" style="background-image: url('<?php echo site_url('assets/img/iconos/BBVA.png');?>');" alt="">
				</div>
				<div class="texto">
					<p>Puntos BBVA</p>
				</div>
			</div>
			<div class="column column-block">
				<div class="img">
					<img src="<?php echo site_url('assets/img/fondo-empresa-home.png');?>" style="background-image: url('<?php echo site_url('assets/img/iconos/sistema-apartado.png');?>');" alt="">
				</div>
				<div class="texto">
					<p>Sistema de apartado</p>
				</div>
			</div>
			<div class="column column-block">
				<div class="img">
					<img src="<?php echo site_url('assets/img/fondo-empresa-home.png');?>" style="background-image: url('<?php echo site_url('assets/img/iconos/si-vale.png');?>');" alt="">
				</div>
				<div class="texto">
					<p>Sí vale</p>
				</div>
			</div>
			<div class="column column-block">
				<div class="img">
					<img src="<?php echo site_url('assets/img/fondo-empresa-home.png');?>" style="background-image: url('<?php echo site_url('assets/img/iconos/broxel.png');?>');" alt="">
				</div>
				<div class="texto">
					<p>Broxel</p>
				</div>
			</div>
		</div>
	</div>
</div>

<footer class="footer-site clearfix">
	<div class="medium-17 columns">
		<div class="medium-4 columns">
			<h5 class="titulo-footer">Menú</h5>
			<ul class="vertical clearfix menu-categorias">
				<li>
					<a href="">
						Productos
					</a>
				</li>
				<li>
					<a href="">
						Promociones
					</a>
				</li>
				<li>
					<a href="">
						Remates
					</a>
				</li>
				<li>
					<a href="">
						Contacto
					</a>
				</li>
			</ul>
		</div>
		<div class="medium-20 columns">
			<h5 class="titulo-footer">Categorías</h5>
			<ul class="menu-categorias clearfix row expanded small-up-2 medium-up-3 large-up-5">
				<li class="column column-block">
					<a href="">
						Pisos
					</a>
				</li>
				<li class="column column-block">
					<a href="">
						Recubrimientos
					</a>
				</li>
				<li class="column column-block">
					<a href="">
						Porcelanatos
					</a>
				</li>
				<li class="column column-block">
					<a href="">
						Descorados
					</a>
				</li>
				<li class="column column-block">
					<a href="">
						Adhesivos
					</a>
				</li>
				<li class="column column-block">
					<a href="">
						Cortadoras
					</a>
				</li>
				<li class="column column-block">
					<a href="">
						Muebles de baño
					</a>
				</li>
				<li class="column column-block">
					<a href="">
						Griferia
					</a>
				</li>
				<li class="column column-block">
					<a href="">
						Tinas
					</a>
				</li>
				<li class="column column-block">
					<a href="">
						Canceles
					</a>
				</li>
				<li class="column column-block">
					<a href="">
						Tinacos
					</a>
				</li>
				<li class="column column-block">
					<a href="">
						Bombas
					</a>
				</li>
				<li class="column column-block">
					<a href="">
						Calentadores
					</a>
				</li>
				<li class="column column-block">
					<a href="">
						Tarjas
					</a>
				</li>
				<li class="column column-block">
					<a href="">
						Parrillas y campanas
					</a>
				</li>
				<li class="column column-block">
					<a href="">
						Puertas y marcos
					</a>
				</li>
				<li class="column column-block">
					<a href="">
						Accesorios y ferretería
					</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="medium-7 columns info-contacto">
		<div class="medium-16 columns sin">
			<p>
				Le esperamos en <br>
				nuestras sucursales:
			</p>
			<a href="tel:+529999460315" class="phone">
				Contáctanos al (999) 946 03 15
			</a>
			<p class="horario">
				Lunes a Viernes de 8:00 am -  7:00 pm
			</p>
			<p class="horario">
				Sabado a Domingo de 8:00 am - 4:00 pm
			</p>
			<div class="row">
				<ul class="contacto menu align-center">
					<li>
						<a href="">
							<span>
								<i class="fab fa-whatsapp"></i>
							</span>
						</a>
					</li>
					<li>
						<a href="">
							<span>
								<i class="fas fa-phone"></i>
							</span>
						</a>
					</li>
					<li>
						<a href="">
							<span>
								<i class="far fa-envelope"></i>
							</span>
						</a>
					</li>
				</ul>
			</div>
		</div>
		<div class="medium-8 columns">
			<div class="img-logo">
				<img src="<?php echo site_url('assets/img/logo-footer.svg');?>" alt="">
			</div>
		</div>
	</div>
	<div class="medium-24 columns copy">
		<p>© Copyright <?php echo $dateTime ?> | Mayoreo cerámico de la península S.A de C.V | TODOS LOS DERECHOS RESERVADOS | 
			<a href="">
				Términos y condiciones.
			</a>
			<a href="">
				Aviso de privacidad
			</a>
			<a href="" class="gant-soluciones-empresariales">
				<img src="<?php echo site_url('assets/img/iconos/gt.svg');?>" alt="">
			</a>
		</p>
	</div>
</footer>