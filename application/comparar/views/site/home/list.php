<?php //print_m($info_home);?>

<section>
	<div class="titulo-site">
		<h1 class="titulo">Nuestros productos</h1>
	</div>

	<div class="categorias categorias-home">
		<div class="row small-up-2 medium-up-3 large-up-4 text-center list-categorias">
			<?php echo $this->load->view('site/home/categorias', '', true); ?>
		</div>
		<div class="btn-site text-center">
			<a href="#" class="button success btn-visualizar-categorias" data-option="0">Ver todos los productos</a>
		</div>
	</div>
</section>

<script>
	$(document)
		.on('click', '.btn-visualizar-categorias', function(e) {
			e.preventDefault();
			var option = $(this).attr('data-option');
			if(option == 0) {
				$('.cont-items').addClass('active');
				$(this).attr('data-option', 1);
			} else{
				$('.cont-items').removeClass('active');
				$(this).attr('data-option', 0);
			}
		})
</script>

<?php if(!empty($info_home['home'][0]->img_secundario_dir)): ?>
<div class="banner banner-home-2">
	<div class="fondo">
		<img src="<?php echo site_url('assets/img/fondo-banner-home-2.png');?>" style="background-image: url('<?php echo $info_home['home'][0]->img_secundario_dir.$info_home['home'][0]->img_secundario_name.' (large).'.$info_home['home'][0]->img_secundario_ext?>');" alt="">
	</div>
</div>
<?php endif ?>

<div class="titulo-site">
	<h1 class="titulo">Los más vendidos</h1>
</div>
<div class="cien">
	<div class="row lista-productos slick-productos">
		<?php for($i = 0; $i < 3; $i++): ?>
		<div class="cont-productos">
			<div class="info-producto">
				<div class="img">
					<img src="<?php echo site_url('assets/img/fondo-producto-item.png');?>" style="background-image: url('<?php echo site_url('assets/img/prueba/producto/piso.png');?>');" alt="">
					<div class="info">
						<div class="img-info">
							<?php echo file_get_contents(site_url('assets/img/iconos/flag.svg')); ?>
						</div>
					</div>
				</div>
				<div class="texto clearfix">
					<h1 class="titulo">Optima navia</h1>
					<p>
						Piso Cerámico Beige / Gris <br>
						33x33 cm - Semibrillante
					</p>
					<p class="precio">
						Desde: $10,299.00 por m2
					</p>
					<span class="codigo">Art. 5590 / 5563</span>
				</div>
			</div>
			<div class="text-center btn">
				<a href="" class="button expanded success">Ver detalles</a>
			</div>
		</div>

		<div class="cont-productos">
			<div class="info-producto">
				<div class="img clearfix">
					<img src="<?php echo site_url('assets/img/fondo-producto-item.png');?>" style="background-image: url('<?php echo site_url('assets/img/prueba/producto/basin.png');?>');" alt="">
				</div>
				<div class="texto clearfix">
					<h1 class="titulo">Optima navia</h1>
					<p>
						Piso Cerámico Beige / Gris <br>
						33x33 cm - Semibrillante
					</p>
					<p class="precio">
						Desde: $299.00 por m2
					</p>
					<span class="codigo">Art. 5590 / 5563</span>
				</div>
			</div>
			<div class="text-center btn">
				<a href="" class="button expanded success">Ver detalles</a>
			</div>
		</div>
		<?php endfor ?>
	</div>
</div>	

<div class="titulo-site">
	<h1 class="titulo">productos del rey</h1>
</div>

<div class="cien">
	<div class="row lista-productos slick-productos">
		<?php for($i = 0; $i < 3; $i++): ?>
		<div class="cont-productos">
			<div class="info-producto">
				<div class="descuento">
					<div class="img">
						<img src="<?php echo site_url('assets/img/iconos/descuento.svg'); ?>" alt="">
					</div>
				</div>
				<div class="corona">
					<div class="img">
						<img src="<?php echo site_url('assets/img/iconos/corona.png'); ?>" alt="">
					</div>
				</div>
				<div class="img">
					<img src="<?php echo site_url('assets/img/fondo-producto-item.png');?>" style="background-image: url('<?php echo site_url('assets/img/prueba/producto/piso.png');?>');" alt="">
					<div class="info">
						<div class="img-info">
							<?php echo file_get_contents(site_url('assets/img/iconos/flag.svg')); ?>
						</div>
					</div>
				</div>
				<div class="texto clearfix">
					<h1 class="titulo">Optima navia</h1>
					<p>
						Piso Cerámico Beige / Gris <br>
						33x33 cm - Semibrillante
					</p>
					<p class="precio">
						Desde: $10,299.00 por m2
					</p>
					<span class="codigo">Art. 5590 / 5563</span>
				</div>
			</div>
			<div class="text-center btn">
				<a href="" class="button expanded success">Ver detalles</a>
			</div>
		</div>

		<div class="cont-productos">
			<div class="info-producto">
				<div class="img clearfix">
					<img src="<?php echo site_url('assets/img/fondo-producto-item.png');?>" style="background-image: url('<?php echo site_url('assets/img/prueba/producto/basin.png');?>');" alt="">
				</div>
				<div class="texto clearfix">
					<h1 class="titulo">Optima navia</h1>
					<p>
						Piso Cerámico Beige / Gris <br>
						33x33 cm - Semibrillante
					</p>
					<p class="precio">
						Desde: $299.00 por m2
					</p>
					<span class="codigo">Art. 5590 / 5563</span>
				</div>
			</div>
			<div class="text-center btn">
				<a href="" class="button expanded success">Ver detalles</a>
			</div>
		</div>
		<?php endfor ?>
	</div>
</div>
<div class="titulo-site">
	<h1 class="titulo">Los consejos del rey</h1>
</div>
<div class="cien">
	<div class="row lista-videos slick-videos">
		<?php for($i = 0; $i < 3; $i++): ?>
		<div class="cont-videos">
			<a data-fancybox href="https://www.youtube.com/watch?v=oKMwQ9oP5EA">
				<div class="img">
					<img src="<?php echo site_url('assets/img/fondo-video-home.png');?>" style="background-image: url('<?php echo site_url('assets/img/prueba/videos/video.jpg');?>');" alt="">
					<div class="hover">
						<i class="far fa-play-circle"></i>
					</div>
				</div>
				<div class="texto">
					<p>
						<img src="<?php echo site_url('assets/img/iconos/push-pin.svg');?>" alt=""> Descubre las diferencias entre pisos y recubrimientos.
					</p>
				</div>
			</a>
		</div>
		<?php endfor ?>
	</div>
</div>

<?php if(!empty($info_home['home'][0]->img_terciario_dir)): ?>
<div class="banner banner-home-2">
	<div class="fondo">
		<img src="<?php echo site_url('assets/img/fondo-banner-home-22.png');?>" style="background-image: url('<?php echo $info_home['home'][0]->img_terciario_dir.$info_home['home'][0]->img_terciario_name.' (large).'.$info_home['home'][0]->img_terciario_ext?>');" alt="">
	</div>
</div>
<?php endif ?>

<script>
	$(document)
		.on('ready', function() {
			$('.slick-productos').slick({
				prevArrow: '<button type="button" class="slick-prev"><img src="<?php echo site_url('assets/img/iconos/left.svg');?>" alt=""></button>',
		        nextArrow: '<button type="button" class="slick-next"><img src="<?php echo site_url('assets/img/iconos/right.svg');?>" alt=""></button>',
				slidesToShow: 4,
				slidesToScroll: 1,
				autoplay: false,
				centerPadding: '60px',
				autoplaySpeed: 5000,
			});

			$('.slick-videos').slick({
				prevArrow: '<button type="button" class="slick-prev"><img src="<?php echo site_url('assets/img/iconos/left.svg');?>" alt=""></button>',
		        nextArrow: '<button type="button" class="slick-next"><img src="<?php echo site_url('assets/img/iconos/right.svg');?>" alt=""></button>',
				slidesToShow: 2,
				slidesToScroll: 1,
				autoplay: false,
				centerPadding: '60px',
				autoplaySpeed: 5000,
			});

			$('.slick-empresas').slick({
				prevArrow: '<button type="button" class="slick-prev"><img src="<?php echo site_url('assets/img/iconos/left.svg');?>" alt=""></button>',
		        nextArrow: '<button type="button" class="slick-next"><img src="<?php echo site_url('assets/img/iconos/right.svg');?>" alt=""></button>',
				slidesToShow: 4,
				slidesToScroll: 1,
				autoplay: true,
				centerPadding: '60px',
				autoplaySpeed: 5000,
			});
		});
</script>

<div class="row quienes-somos-home">
	<div class="large-12 columns texto">
		<h1 class="titulo">¿quiÉnes somos?</h1>
		<p>
			En enero de 2003 inicia operaciones la
			empresa con el nombre de Recubre
			Sureste S.A. de C.V. aperturando en la
			Ciudad de Cancún Quintana Roo.
		</p>
		<p>
			En agosto de ese mismo año se traslada 
			a empresa a la Ciudad de Mérida en la
			colonia cuidad industrial cambiando el
			nombre a Pisos y más del sureste, en ese
			entonces conformado por 5 personas.
		</p>
	</div>
	<div class="large-12 columns img">

		<?php if(!empty($info_home['home'][0]->img_quienes_dir)): ?>
		<img src="<?php echo site_url('assets/img/fondo-quienes-somos.png');?>" style="background-image: url('<?php echo $info_home['home'][0]->img_quienes_dir.$info_home['home'][0]->img_quienes_name.' (large).'.$info_home['home'][0]->img_quienes_ext?>');" alt="">
		<?php endif ?>
	</div>
</div>
<div class="cien">
	<div class="row lista-empresas slick-empresas">
		<?php for($i = 1; $i <= 5; $i++): ?>
			<div class="cont-empresas">
				<div class="img">
					<img src="<?php echo site_url('assets/img/fondo-empresa-home.png');?>" style="background-image: url('<?php echo site_url('assets/img/prueba/empresas/'.$i.'.png');?>');" alt="">
				</div>
			</div>
		<?php endfor?>
	</div>
</div>
<div class="row contacto">
	<div class="titulo-contacto">
		<h1 class="titulo">
			estamos ubicados en mérida
			<span>¡Le esperamooos!</span>
		</h1>
		<div class="row cont-contacto">
			<div class="large-13 columns">
				<h4 class="titulo-contacto">
					Sucursales 
					<img src="<?php echo site_url('assets/img/iconos/shop.svg');?>" alt="">
				</h4>
				<div class="row texto-contacto">
					<div class="large-8 columns">
						<h6 class="titulo-texto">CEDIS</h6>
						<a class="telefono" href="tel:+529999460315">
							<img src="<?php echo site_url('assets/img/iconos/telefono.png');?>" alt="">
							Tel: (999) 946 03 15
						</a>
						<p class="direccion">
							PERIFERICO SUR KM 1 A 300 MTS<br> 
							DE LA ACADEMIA DE POLICIAS <br>
							C.P 97255
						</p>
						<p>
							Mérida, Yucatán, México.
						</p>
						<a href="mailto:pisoventas.cedis@superpisosmcp.com">pisoventas.cedis@superpisosmcp.com</a>
						<a href="mailto:ventas.mayoreo@superpisosmcp.com">ventas.mayoreo@superpisosmcp.com</a>
					</div>
					<div class="large-8 columns">
						<h6 class="titulo-texto">Arco Dragones</h6>
						<a class="telefono" href="tel:+529999234802">
							<img src="<?php echo site_url('assets/img/iconos/telefono.png');?>" alt="">
							Tel: (999) 923 48 02
						</a>
						<p class="direccion">
							Calle 50 x 61 Col. Centro
						</p>
						<p>
							Mérida, Yucatán, México.
						</p>
						<a href="mailto:sucursaldragones@superpisosmcp.com">sucursaldragones@superpisosmcp.com</a>
					</div>
					<div class="large-8 columns">
						<h6 class="titulo-texto">60 NORTE</h6>
						<a class="telefono" href="tel:+529999277223">
							<img src="<?php echo site_url('assets/img/iconos/telefono.png');?>" alt="">
							Tel: (999) 927 72 23
						</a>
						<p class="direccion">
							Calle 60 X CIRCUITO COLONIAS
						</p>
						<p>
							Mérida, Yucatán, México.
						</p>
						<a href="mailto:suc60norte@superpisosmcp.com">suc60norte@superpisosmcp.com</a>
					</div>
				</div>
				<h4 class="titulo-contacto">
					envíanos un correo 
					<img src="<?php echo site_url('assets/img/iconos/mail.svg');?>" alt="">
				</h4>
				<div class="form">
					<form action="" method="post">
						<div class="row">
							<div class="small-12 columns">
								<label>
									<input type="text" placeholder="Nombres">
								</label>
							</div>
							<div class="small-12 columns">
								<label>
									<input type="text" placeholder="Apellidos">
								</label>
							</div>
							<div class="small-24 columns">
								<label>
									<input type="text" placeholder="E-mail">
								</label>
							</div>
							<div class="small-4 columns">
								<label>
									<input type="text" placeholder="+52">
								</label>
							</div>
							<div class="small-20 columns">
								<label>
									<input type="text" placeholder="Teléfono">
								</label>
							</div>
							<div class="small-24 columns">
								<label>
									<textarea placeholder="Mensaje"></textarea>
								</label>
							</div>
							<div class="small-24 columns">
								<button type="button" class="success button">Enviar</button>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="large-11 columns mapa">
				<div id="mapa"></div>
			</div>
		</div>
	</div>
</div>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDKF75u1uI_LgNOqJTpIwvM3bnkimIEqoQ&callback=initialize" async defer></script>
<script>
	var styleArray = 
	[{
		stylers: [
			{ hue: "#c78336" },
			{ saturation: "-20" },
			{ visibility: "simplified" }
		]
	}];
	function initialize() {
		var myLatlng = new google.maps.LatLng(21.043629, -89.628137);
		var imagePath = '<?php echo site_url('assets/img/pin.png');?>'
		var mapOptions = {
			zoom: 17,
			scrollwheel: false,
			center: myLatlng,
			styles: styleArray,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		}
		var map = new google.maps.Map(document.getElementById('mapa'), mapOptions);
		var contentString = 'Samline';
		var infowindow = new google.maps.InfoWindow({
			content: contentString,
			maxWidth: 500
		});
		var marker = new google.maps.Marker({
			position: myLatlng,
			map: map,
			icon: imagePath,
			title: 'image title'
		});
		google.maps.event.addListener(marker, 'click', function() {
			infowindow.open(map,marker);
		});
		google.maps.event.addDomListener(window, "resize", function() {
			var center = map.getCenter();
			google.maps.event.trigger(map, "resize");
			map.setCenter(center);
		});
	}
</script>