<div class="carrito">
    <div class="titulo">
        <h3>Esta a Punto de Renovar</h3>
        <p>Algo Nuevo en su Espacio.</p>
    </div>
    <div class="contenido">
        <div class="subitulo">
            <h4>Carrito de Cotización</h4>
        </div>
        <div class="grid">
            <div id="izqCarrito" class="izq">
                <div class="producto">
                    <div class="contenido-producto">
                        <div class="articulo">
                            <p class="header">Articulo</p>
                            <div class="body">
                                <div class="despliegue">
                                    <div class="imagen">
                                        <img src="<?= site_url('assets/img/fondo-producto-item.png');?>" style="background-image: url('<?= site_url('assets/img/prueba/producto/piso.png');?>');" alt="">
                                    </div>
                                    <div class="detalles">
                                        <p class="nombre">Optima Navia</p>
                                        <p class="desc">fefewfewfeffe fewfewfe fewfewfewfewf</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cantidad">
                            <p class="header">Cantidad</p>
                            <div class="body">
                                <div class="sumadorProductos">
                                    <button class="menos">-</button>
                                    <input type="text" class="cantidad" value="10">
                                    <button  class="mas">+</button>
                                </div>
                            </div>
                        </div>
                        <div class="cancelar">
                            <a href="#"><p class="header"><img src="<?= site_url('assets/img/iconos/right.svg');?>">Seguir Cotizando</p></a>
                            <div class="body">
                                <button class="btn-cancelar">
                                    <img src="<?= site_url('assets/img/iconos/cancel-product.svg');?>">
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="producto">
                    <div class="contenido-producto">
                        <div class="articulo">
                            <p class="header">Articulo</p>
                            <div class="body">
                                <div class="despliegue">
                                    <div class="imagen">
                                        <img src="<?= site_url('assets/img/fondo-producto-item.png');?>" style="background-image: url('<?= site_url('assets/img/prueba/producto/sillas.jpg');?>');" alt="">
                                    </div>
                                    <div class="detalles">
                                        <p class="nombre">Optima Navia</p>
                                        <p class="desc">fefewfewfeffe fewfewfe fewfewfewfewf</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cantidad">
                            <p class="header">Cantidad</p>
                            <div class="body">
                                <div class="sumadorProductos">
                                    <button class="menos">-</button>
                                    <input type="text" class="cantidad" value="10">
                                    <button  class="mas">+</button>
                                </div>
                            </div>
                        </div>
                        <div class="cancelar">
                            <div class="body">
                                <button class="btn-cancelar">
                                    <img src="<?= site_url('assets/img/iconos/cancel-product.svg');?>">
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="producto">
                    <div class="contenido-producto">
                        <div class="articulo">
                            <p class="header">Articulo</p>
                            <div class="body">
                                <div class="despliegue">
                                    <div class="imagen">
                                        <img src="<?= site_url('assets/img/fondo-producto-item.png');?>" style="background-image: url('<?= site_url('assets/img/prueba/producto/cruzetas.png');?>');" alt="">
                                    </div>
                                    <div class="detalles">
                                        <p class="nombre">Optima Navia</p>
                                        <p class="desc">fefewfewfeffe fewfewfe fewfewfewfewf</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cantidad">
                            <p class="header">Cantidad</p>
                            <div class="body">
                                <div class="sumadorProductos">
                                    <button class="menos">-</button>
                                    <input type="text" class="cantidad" value="10">
                                    <button  class="mas">+</button>
                                </div>
                            </div>
                        </div>
                        <div class="cancelar">
                            <div class="body">
                                <button class="btn-cancelar">
                                    <img src="<?= site_url('assets/img/iconos/cancel-product.svg');?>">
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="producto">
                    <div class="contenido-producto">
                        <div class="articulo">
                            <p class="header">Articulo</p>
                            <div class="body">
                                <div class="despliegue">
                                    <div class="imagen">
                                        <img src="<?= site_url('assets/img/fondo-producto-item.png');?>" style="background-image: url('<?= site_url('assets/img/prueba/producto/costales.jpg');?>');" alt="">
                                    </div>
                                    <div class="detalles">
                                        <p class="nombre">Optima Navia</p>
                                        <p class="desc">fefewfewfeffe fewfewfe fewfewfewfewf</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cantidad">
                            <p class="header">Cantidad</p>
                            <div class="body">
                                <div class="sumadorProductos">
                                    <button class="menos">-</button>
                                    <input type="text" class="cantidad" value="10">
                                    <button  class="mas">+</button>
                                </div>
                            </div>
                        </div>
                        <div class="cancelar">
                            <div class="body">
                                <button class="btn-cancelar">
                                    <img src="<?= site_url('assets/img/iconos/cancel-product.svg');?>">
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="producto">
                    <div class="contenido-producto">
                        <div class="articulo">
                            <p class="header">Articulo</p>
                            <div class="body">
                                <div class="despliegue">
                                    <div class="imagen">
                                        <img src="<?= site_url('assets/img/fondo-producto-item.png');?>" style="background-image: url('<?= site_url('assets/img/prueba/producto/cortadora.png');?>');" alt="">
                                    </div>
                                    <div class="detalles">
                                        <p class="nombre">Optima Navia</p>
                                        <p class="desc">fefewfewfeffe fewfewfe fewfewfewfewf</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cantidad">
                            <p class="header">Cantidad</p>
                            <div class="body">
                                <div class="sumadorProductos">
                                    <button class="menos">-</button>
                                    <input type="text" class="cantidad" value="10">
                                    <button  class="mas">+</button>
                                </div>
                            </div>
                        </div>
                        <div class="cancelar">
                            <div class="body">
                                <button class="btn-cancelar">
                                    <img src="<?= site_url('assets/img/iconos/cancel-product.svg');?>">
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="producto">
                    <div class="contenido-producto">
                        <div class="articulo">
                            <p class="header">Articulo</p>
                            <div class="body">
                                <div class="despliegue">
                                    <div class="imagen">
                                        <img src="<?= site_url('assets/img/fondo-producto-item.png');?>" style="background-image: url('<?= site_url('assets/img/prueba/producto/basin.png');?>');" alt="">
                                    </div>
                                    <div class="detalles">
                                        <p class="nombre">Optima Navia</p>
                                        <p class="desc">fefewfewfeffe fewfewfe fewfewfewfewf</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cantidad">
                            <p class="header">Cantidad</p>
                            <div class="body">
                                <div class="sumadorProductos">
                                    <button class="menos">-</button>
                                    <input type="text" class="cantidad" value="10">
                                    <button  class="mas">+</button>
                                </div>
                            </div>
                        </div>
                        <div class="cancelar">
                            <div class="body">
                                <button class="btn-cancelar">
                                    <img src="<?= site_url('assets/img/iconos/cancel-product.svg');?>">
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="producto">
                    <div class="contenido-producto">
                        <div class="articulo">
                            <p class="header">Articulo</p>
                            <div class="body">
                                <div class="despliegue">
                                    <div class="imagen">
                                        <img src="<?= site_url('assets/img/fondo-producto-item.png');?>" style="background-image: url('<?= site_url('assets/img/prueba/producto/piso.png');?>');" alt="">
                                    </div>
                                    <div class="detalles">
                                        <p class="nombre">Optima Navia</p>
                                        <p class="desc">fefewfewfeffe fewfewfe fewfewfewfewf</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="cantidad">
                            <p class="header">Cantidad</p>
                            <div class="body">
                                <div class="sumadorProductos">
                                    <button class="menos">-</button>
                                    <input type="text" class="cantidad" value="10">
                                    <button  class="mas">+</button>
                                </div>
                            </div>
                        </div>
                        <div class="cancelar">
                            <div class="body">
                                <button class="btn-cancelar">
                                    <img src="<?= site_url('assets/img/iconos/cancel-product.svg');?>">
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <button class="descargar">Descargar Cotización</button>
            </div>
            <div class="der" data-sticky-container>
                <div class="sticky" data-sticky data-margin-top="8" data-anchor="izqCarrito">
                    <img src="<?= site_url('assets/img/banners/carrito/banner.png');?>" alt="">
                </div>
            </div>
        </div>
    </div>
</div>

<script>
	$(document).ready(function(){

        $(".btn-cancelar").click(function(){
            let productos = $(".producto");

            $(this).parent().parent().parent().parent().remove();
        });

        $(".sumadorProductos").each(function (index, element) {
            let sumador = element;
            let menos = $(sumador).find(".menos");
            let mas = $(sumador).find(".mas");
            let cantidad = $(sumador).find(".cantidad");

            let contador = 1,
                min = 1,
                max = 9999;


            cambiarCantidad();

            function cambiarCantidad() {
                $(cantidad).val(contador);
            };

            $(cantidad).on("change", function(){
                let val = $(cantidad).val();
                if(val < min){
                    contador = min;
                }else if(val > max){
                    contador = max;
                }else if(isNaN(val)){
                    contador = min;
                }else{
                    contador = val; 
                }
            
                $(cantidad).val(contador);
            });

            $(mas).on('click touch', function() {
                if(contador < max){
                    contador++;
                    cambiarCantidad();
                }
            });

            $(menos).on('click touch', function() {
                if(contador > min){
                    contador--;
                    cambiarCantidad();
                }
            });
            
        });
	});
</script>

<div class="titulo-site">
	<h1 class="titulo">Productos Relacionados</h1>
</div>

<div class="row lista-productos slick-productos">
	<?php for($i = 0; $i < 3; $i++): ?>
	<div class="cont-productos">
		<div class="info-producto">
			<div class="img">
				<img src="<?php echo site_url('assets/img/fondo-producto-item.png');?>" style="background-image: url('<?php echo site_url('assets/img/prueba/producto/piso.png');?>');" alt="">
				<div class="info">
					<div class="img-info">
						<?php echo file_get_contents(site_url('assets/img/iconos/flag.svg')); ?>
					</div>
				</div>
			</div>
			<div class="texto clearfix">
				<h1 class="titulo">Optima navia</h1>
				<p>
					Piso Cerámico Beige / Gris <br>
					33x33 cm - Semibrillante
				</p>
				<p class="precio">
					Desde: $10,299.00 por m2
				</p>
				<span class="codigo">Art. 5590 / 5563</span>
			</div>
		</div>
		<div class="text-center btn">
			<a href="" class="button expanded success">Ver detalles</a>
		</div>
	</div>

	<div class="cont-productos">
		<div class="info-producto">
			<div class="img clearfix">
				<img src="<?php echo site_url('assets/img/fondo-producto-item.png');?>" style="background-image: url('<?php echo site_url('assets/img/prueba/producto/costales.jpg');?>');" alt="">
			</div>
			<div class="texto clearfix">
				<h1 class="titulo">Optima navia</h1>
				<p>
					Piso Cerámico Beige / Gris <br>
					33x33 cm - Semibrillante
				</p>
				<p class="precio">
					Desde: $299.00 por m2
				</p>
				<span class="codigo">Art. 5590 / 5563</span>
			</div>
		</div>
		<div class="text-center btn">
			<a href="" class="button expanded success">Ver detalles</a>
		</div>
	</div>

	<div class="cont-productos">
		<div class="info-producto">
			<div class="img">
				<img src="<?php echo site_url('assets/img/fondo-producto-item.png');?>" style="background-image: url('<?php echo site_url('assets/img/prueba/producto/cortadora.png');?>');" alt="">
				<div class="info">
					<div class="img-info">
						<?php echo file_get_contents(site_url('assets/img/iconos/flag.svg')); ?>
					</div>
				</div>
			</div>
			<div class="texto clearfix">
				<h1 class="titulo">Optima navia</h1>
				<p>
					Piso Cerámico Beige / Gris <br>
					33x33 cm - Semibrillante
				</p>
				<p class="precio">
					Desde: $10,299.00 por m2
				</p>
				<span class="codigo">Art. 5590 / 5563</span>
			</div>
		</div>
		<div class="text-center btn">
			<a href="" class="button expanded success">Ver detalles</a>
		</div>
	</div>

	<div class="cont-productos">
		<div class="info-producto">
			<div class="img">
				<img src="<?php echo site_url('assets/img/fondo-producto-item.png');?>" style="background-image: url('<?php echo site_url('assets/img/prueba/producto/cruzetas.png');?>');" alt="">
				<div class="info">
					<div class="img-info">
						<?php echo file_get_contents(site_url('assets/img/iconos/flag.svg')); ?>
					</div>
				</div>
			</div>
			<div class="texto clearfix">
				<h1 class="titulo">Optima navia</h1>
				<p>
					Piso Cerámico Beige / Gris <br>
					33x33 cm - Semibrillante
				</p>
				<p class="precio">
					Desde: $10,299.00 por m2
				</p>
				<span class="codigo">Art. 5590 / 5563</span>
			</div>
		</div>
		<div class="text-center btn">
			<a href="" class="button expanded success">Ver detalles</a>
		</div>
	</div>

	<?php endfor ?>
</div>

<script>
	$(document)
		.on('ready', function() {
			$('.slick-productos').slick({
				prevArrow: '<button type="button" class="slick-prev"><img src="<?php echo site_url('assets/img/iconos/left.svg');?>" alt=""></button>',
		        nextArrow: '<button type="button" class="slick-next"><img src="<?php echo site_url('assets/img/iconos/right.svg');?>" alt=""></button>',
				slidesToShow: 4,
				slidesToScroll: 1,
				autoplay: false,
				centerPadding: '60px',
				autoplaySpeed: 5000,
			});
		});
</script>