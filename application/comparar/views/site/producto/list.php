<div class="vistaProducto">
	<div class="contenido">
		<div class="foto">
			<div class="imagen">
				<img src="<?= site_url('assets/img/fondo-producto-item.png');?>" style="background-image: url('<?= site_url('assets/img/prueba/producto/piso.png');?>');" alt="">
			</div>
			<p class="descImagen">*Fotografia Ilustrativa</p>
			<button class="compartir"><img class="share" src="<?= site_url('assets/img/iconos/share.svg');?>" alt=""> <span>Compartir</span> <img src="<?= site_url('assets/img/iconos/whatsapp-color.svg');?>" alt=""><img src="<?= site_url('assets/img/iconos/facebook-color.svg');?>" alt=""></button>
		</div>	
		<div class="descProducto">
			<div class="opciones">
				<h3 class="tituloProducto">Optima Navia</h3>
				<div class="tamano">
					<p>PEI: 3</p>
					<p>Tamaño:  33x33 cm</p>
				</div>
				<div class="precio">
					<p class="desde">Desde: $77.95 por m2</p>
					<div id="sumadorProductos">
						<button class="menos">-</button>
						<input type="text" class="cantidad" value="10">
						<button  class="mas">+</button>
					</div>
				</div>
				<div class="detalles">
					<div class="detalle">
						<p>Marca</p>
						<p>Daltile</p>
					</div>
					<div class="detalle">
						<p>Material</p>
						<p>Piso cerámico</p>
					</div>
					<div class="detalle">
						<p>Acabado</p>
						<p>Semi brillante</p>
					</div>
				</div>
				<button class="anadir" >Añadir a la bolsa</button>
			</div>
			<div class="descripcion">
				<p class="titulo">Descripción</p>
				<p class="fullDescProducto">Piso cerámico tipo marmoleado semibrillante Navia
					beige en 33x33 cm. en piso y 20x30 en muro
				</p>
				<p class="codigo">Art. 5590/5563</p>
			</div>
		</div>
	</div>
</div>

<script>
	$(document).ready(function(){
		let sumador = $("#sumadorProductos");
		let menos = $(sumador).find(".menos");
		let mas = $(sumador).find(".mas");
		let cantidad = $(sumador).find(".cantidad");

		let contador = 1,
			min = 1,
			max = 9999;


		cambiarCantidad();

		function cambiarCantidad() {
			$(cantidad).val(contador);
		};

		$(cantidad).on("change", function(){
			let val = $(cantidad).val();
			if(val < min){
				contador = min;
			}else if(val > max){
				contador = max;
			}else if(!Number.isInteger(val)){
				contador = min;
			}else{
				contador = val; 
			}
		
			$(cantidad).val(contador);
		});

		$(mas).on('click touch', function() {
			if(contador < max){
				contador++;
				cambiarCantidad();
			}
		});

		$(menos).on('click touch', function() {
			if(contador > min){
				contador--;
				cambiarCantidad();
			}
		});
	});
</script>

<div class="tabul">
	<div class="contenidoTab">
		<ul class="tabs" data-tabs id="collapsing-tabs">
			<li class="tabs-title is-active"><a href="#panel1" aria-selected="true">Especificaciones</a></li>
			<li class="tabs-title"><a href="#panel2">Usos</a></li>
			<li class="tabs-title"><a href="#panel3">Áreas de Aplicación</a></li>
		</ul>

		<div class="tabs-content" data-tabs-content="collapsing-tabs">
			<div class="tabs-panel is-active" id="panel1">
				<div class="contenido">
					<div class="imagen">
						<img src="<?= site_url('assets/img/prueba/producto/sillas.jpg');?>" alt="">
					</div>
					<div class="texto">
						<ul>
							<li>NUMERO 1</li>
							<li>Apariencia estetica que se mantienen en tendencia</li>
							<li>Diseño que se adapta a cualquier espacio</li>
							<li>resistencia en ambientes exteriores y areas humedas</li>
							<li>Variedad de acabados: pulido, brillante, mate o ceroso</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="tabs-panel" id="panel2">
			<div class="contenido">
					<div class="imagen">
						<img src="<?= site_url('assets/img/prueba/producto/sillas.jpg');?>" alt="">
					</div>
					<div class="texto">
						<ul>
							<li>NUMERO 2</li>
							<li>Apariencia estetica que se mantienen en tendencia</li>
							<li>Diseño que se adapta a cualquier espacio</li>
							<li>resistencia en ambientes exteriores y areas humedas</li>
							<li>Variedad de acabados: pulido, brillante, mate o ceroso</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="tabs-panel" id="panel3">
			<div class="contenido">
					<div class="imagen">
						<img src="<?= site_url('assets/img/prueba/producto/sillas.jpg');?>" alt="">
					</div>
					<div class="texto">
						<ul>
							<li>NUMERO 3</li>
							<li>Apariencia estetica que se mantienen en tendencia</li>
							<li>Diseño que se adapta a cualquier espacio</li>
							<li>resistencia en ambientes exteriores y areas humedas</li>
							<li>Variedad de acabados: pulido, brillante, mate o ceroso</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>




<div class="titulo-site">
	<h1 class="titulo">Productos Relacionados</h1>
</div>

<div class="row lista-productos slick-productos">
	<?php for($i = 0; $i < 3; $i++): ?>
	<div class="cont-productos">
		<div class="info-producto">
			<div class="img">
				<img src="<?php echo site_url('assets/img/fondo-producto-item.png');?>" style="background-image: url('<?php echo site_url('assets/img/prueba/producto/piso.png');?>');" alt="">
				<div class="info">
					<div class="img-info">
						<?php echo file_get_contents(site_url('assets/img/iconos/flag.svg')); ?>
					</div>
				</div>
			</div>
			<div class="texto clearfix">
				<h1 class="titulo">Optima navia</h1>
				<p>
					Piso Cerámico Beige / Gris <br>
					33x33 cm - Semibrillante
				</p>
				<p class="precio">
					Desde: $10,299.00 por m2
				</p>
				<span class="codigo">Art. 5590 / 5563</span>
			</div>
		</div>
		<div class="text-center btn">
			<a href="" class="button expanded success">Ver detalles</a>
		</div>
	</div>

	<div class="cont-productos">
		<div class="info-producto">
			<div class="img clearfix">
				<img src="<?php echo site_url('assets/img/fondo-producto-item.png');?>" style="background-image: url('<?php echo site_url('assets/img/prueba/producto/costales.jpg');?>');" alt="">
			</div>
			<div class="texto clearfix">
				<h1 class="titulo">Optima navia</h1>
				<p>
					Piso Cerámico Beige / Gris <br>
					33x33 cm - Semibrillante
				</p>
				<p class="precio">
					Desde: $299.00 por m2
				</p>
				<span class="codigo">Art. 5590 / 5563</span>
			</div>
		</div>
		<div class="text-center btn">
			<a href="" class="button expanded success">Ver detalles</a>
		</div>
	</div>

	<div class="cont-productos">
		<div class="info-producto">
			<div class="img">
				<img src="<?php echo site_url('assets/img/fondo-producto-item.png');?>" style="background-image: url('<?php echo site_url('assets/img/prueba/producto/cortadora.png');?>');" alt="">
				<div class="info">
					<div class="img-info">
						<?php echo file_get_contents(site_url('assets/img/iconos/flag.svg')); ?>
					</div>
				</div>
			</div>
			<div class="texto clearfix">
				<h1 class="titulo">Optima navia</h1>
				<p>
					Piso Cerámico Beige / Gris <br>
					33x33 cm - Semibrillante
				</p>
				<p class="precio">
					Desde: $10,299.00 por m2
				</p>
				<span class="codigo">Art. 5590 / 5563</span>
			</div>
		</div>
		<div class="text-center btn">
			<a href="" class="button expanded success">Ver detalles</a>
		</div>
	</div>

	<div class="cont-productos">
		<div class="info-producto">
			<div class="img">
				<img src="<?php echo site_url('assets/img/fondo-producto-item.png');?>" style="background-image: url('<?php echo site_url('assets/img/prueba/producto/cruzetas.png');?>');" alt="">
				<div class="info">
					<div class="img-info">
						<?php echo file_get_contents(site_url('assets/img/iconos/flag.svg')); ?>
					</div>
				</div>
			</div>
			<div class="texto clearfix">
				<h1 class="titulo">Optima navia</h1>
				<p>
					Piso Cerámico Beige / Gris <br>
					33x33 cm - Semibrillante
				</p>
				<p class="precio">
					Desde: $10,299.00 por m2
				</p>
				<span class="codigo">Art. 5590 / 5563</span>
			</div>
		</div>
		<div class="text-center btn">
			<a href="" class="button expanded success">Ver detalles</a>
		</div>
	</div>

	<?php endfor ?>
</div>

<script>
	$(document)
		.on('ready', function() {
			$('.slick-productos').slick({
				prevArrow: '<button type="button" class="slick-prev"><img src="<?php echo site_url('assets/img/iconos/left.svg');?>" alt=""></button>',
		        nextArrow: '<button type="button" class="slick-next"><img src="<?php echo site_url('assets/img/iconos/right.svg');?>" alt=""></button>',
				slidesToShow: 4,
				slidesToScroll: 1,
				autoplay: false,
				centerPadding: '60px',
				autoplaySpeed: 5000,
			});
		});
</script>