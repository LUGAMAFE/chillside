<!-- Emails use the XHTML Strict doctype -->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <!-- The character set should be utf-8 -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width"/>
    <!-- Link to the email's CSS, which will be inlined into the email -->
    <?php echo link_tag(array('href' => 'assets/img/icon.png', 'rel' => 'shortcut icon')); ?>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
    <?php $this->load->view('site/template/correo_include'); ?>
  <!-- Wrapper for the body of the email -->
    <table align="center" class="container">
        <tbody>
            <tr>
               <td class="float-center" align="center" valign="top">
                    <table class="row collapse">
                        <tbody>
                            <tr>
                                <th class="small-12 large-12 columns">
                                    <table>
                                        <tr>
                                            <th class="logo">
                                                <img src="<?php echo site_url('assets/img/logo-color.svg');?>" alt="">
                                            </th>
                                        </tr>
                                    </table>
                                </th>
                            </tr>
                        </tbody>
                    </table>
                    <table class="row">
                        <tbody>
                            <tr>
                                <th class="small-12 large-12 columns">
                                    <table>
                                        <tr>
                                            <td class="info-reserva">
                                                <h4 class="titulo">
                                                    Correo de contacto
                                                </h4>
                                                <p>
                                                    Nombre: <b><?php echo $nombre ?></b> 
                                                </p>
                                                <p>
                                                    Email: <b><?php echo $email ?></b> 
                                                </p>
                                                <p>
                                                    Asunto: <b><?php echo $asunto ?></b> 
                                                </p>
                                                <p>
                                                    Teléfono: <b><?php echo $telefono ?></b> 
                                                </p>
                                                <p>
                                                    Mensaje: <b><?php echo $mensaje ?></b> 
                                                </p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <th class="gracias">
                                                <p>
                                                    Correo de contacto
                                                </p>
                                            </th>
                                        </tr>
                                    </table>
                                </th>
                            </tr>
                        </tbody>
                    </table>
                </td> 
            </tr>
            <tr>
                <td>
                    <table class="row" id="footer">
                        <tbody>
                            <tr>
                                <th class="small-12 large-8 columns first">
                                    <table>
                                        <tr>
                                            <th>
                                                <img src="<?php echo site_url('assets/img/logo.svg');?>" id="logo-footer">
                                            </th>
                                        </tr>
                                        <tr>
                                            <th>
                                                <p id="derechos"><span>© GRUPO BARI |</span> TODOS LOS DERECHOS RESERVADOS</p>
                                            </th>
                                        </tr>
                                    </table>
                                </th>
                                <th class="small-12 large-4 columns last">
                                    <table>
                                        <tr>
                                            <th id="redes">
                                                <h4 class="titulo">Síguenos:</h4>
                                                <ul class="menu-redes">
                                                    <li>
                                                        <a href="#" class="redes facebook">
                                                            <span>
                                                                <i class="fa fa-facebook" aria-hidden="true"></i>
                                                            </span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="redes instagram">
                                                            <span>
                                                                <i class="fa fa-instagram" aria-hidden="true"></i>
                                                            </span>
                                                        </a>
                                                    </li>
                                                </ul>
                                                <a href="<?php echo site_url();?>" id="link-page">www.grupobari.com.mx</a>
                                            </th>
                                        </tr>
                                    </table>
                                </th>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</body>
</html>