<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdminProducto extends CI_Controller {

    public function __construct(){
        parent::__construct();
        //$this->load->model('Producto_Model');
        $this->load->helper('url_helper');
        $this->load->helper('path');
    }

    public function home($idProducto){
        $this->load->view('producto');
        // $productos = @file_get_contents('http://slim.test/productosInfo');
        // if($productos == FALSE){
        //     show_404();
        // }else{
        //     $productos = json_decode($productos, true);
        //     if($productos["error"] == TRUE){
        //         show_404();
        //     }else{
        //         $productos = $productos["resultado"];
        //         $productosDB = $this->Productos_Model->getProductosInfo();

        //         $nuevosProductos = array_udiff($productos, $productosDB, "key_compare_func");
        //         $this->Productos_Model->insertarNuevosProductos($nuevosProductos);
                
        //         $interseccion = array_uintersect($productos, $productosDB, "key_compare_func");
        //         $productosCambiosGrupo = array_udiff($interseccion, $productosDB, "key_compare_func2");

        //         $this->Productos_Model->actualizarGrupoProductos($productosCambiosGrupo);

        //         $productosTabla = $this->Productos_Model->obtenerProductosTabla();
        //         foreach ($productosTabla as $productoKey => $productoValue) {
        //             $productosTabla[$productoKey]["Descripcion1"] = $productos[$productoKey]["Descripcion1"];
        //         }

        //         $data["productos"] = $productosTabla;
        //         $this->load->view('productos', $data);
        //     }
        // }
    }


    public function subirImagen(){
        $this->load->library('handlerfineuploader');
	
        $uploader = new Handlerfineuploader();

        // Specify the list of valid extensions, ex. array("jpeg", "xml", "bmp")
        $uploader->allowedExtensions = array('png', 'jpeg', 'jpg', 'gif', 'bmp'); // all files types allowed by default

        // Specify max file size in bytes.
        $uploader->sizeLimit =  1 * 1024 * 1024 * 8;

        // Specify the input name set in the javascript.
        $uploader->inputName = "qqfile"; // matches Fine Uploader's default inputName value by default

        // If you want to use the chunking/resume feature, specify the folder to temporarily save parts.
        $uploader->chunksFolder = '../public/assets/img/productos/chunks';

        // This will retrieve the "intended" request method.  Normally, this is the
        // actual method of the request.  Sometimes, though, the intended request method
        // must be hidden in the parameters of the request.  For example, when attempting to
        // delete a file using a POST request. In that case, "DELETE" will be sent along with
        // the request in a "_method" parameter.
        function get_request_method() {
            global $HTTP_RAW_POST_DATA;

            if(isset($HTTP_RAW_POST_DATA)) {
                parse_str($HTTP_RAW_POST_DATA, $_POST);
            }

            if (isset($_POST["_method"]) && $_POST["_method"] != null) {
                return $_POST["_method"];
            }

            return $_SERVER["REQUEST_METHOD"];
        }

        $method = get_request_method();

        

        if ($method == "POST") {
            header("Content-Type: text/plain");

            // Assumes you have a chunking.success.endpoint set to point here with a query parameter of "done".
            // For example: /myserver/handlers/endpoint.php?done
            if (isset($_GET["done"])) {
                $result = $uploader->combineChunks('../public/assets/img/productos/');
            }
            // Handles upload requests
            else {
                // Call handleUpload() with the name of the folder, relative to PHP's getcwd()
                $result = $uploader->handleUpload('../public/assets/img/productos/');

                // To return a name used for uploaded file you can use the following line.
                $result["uploadName"] = $uploader->getUploadName();
            }

            echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
        }
        // for delete file requests
        else if ($method == "DELETE") {
            $result = $uploader->handleDelete('../public/assets/img/productos/');
            echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
        }
        else {
            header("HTTP/1.0 405 Method Not Allowed");
        }
    }

    public function actualizarVisibilidadProducto(){
        $idProducto = $this->input->post('idProducto');
        $estado = $this->input->post('estado');

        $error = $this->Productos_Model->actualizarVisibilidadProducto($idProducto, $estado);

        $output = ["error" => !$error];

        $json = json_encode($output);
        echo $json;
    }
}