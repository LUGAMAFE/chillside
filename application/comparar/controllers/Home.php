<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MY_Controller {

	protected $class = '';
	protected $folder = '/site';
	protected $folder_set = '/site/partials/';
	protected $object;
	protected $url_recuperar;
	
	public function __construct()
	{
		parent::__construct();
		$this->class = strtolower(get_class());
		$this->load->model('producto');
	}

	public function get_productos() 
	{
		$result = $this->producto->select('Articulo, Descripcion1, Descripcion2')
			->where_in('Grupo', array('Ferreteria', 'Complementos de Baño', 'Decorados', 'Muebles de Baño', 'Griferia', 'Pisos y Recubrimientos', 'Adhesivos', 'Puertas, Marcos y Ventanas', 'Calentadores', 'Decorados, Insertos y Mallas'))
			->where('Estatus', 'Alta')
			->get();
		$this->output->set_header('Content-Encoding: gzip')
                 ->set_header('Vary: Accept-Encoding')
                 ->set_content_type('application/json')
                 ->set_output(gzencode(json_encode($result->result()), 9));
	}

	public function index($url = '')
	{
		/*$newdata = array(
			'username'  => 'lugamafe',
			'email'     => 'luisjavier004@hotmail.com',
			'logged_in' => TRUE,
		);
		$this->session->set_userdata($newdata);*/
		$this->load->model('administracion/pagina/home_model');
		$datos['info_home'] = $this->home_model->get_home();
		$datos['class'] = $this->class;
		$banner['banner'] = 1;
		$datos['active'] = 'inicio';
		$this->template->write('title', 'Home');
		$this->template->asset_js('fancybox/dist/jquery.fancybox.min.js');
		$this->template->asset_css('fancybox/dist/jquery.fancybox.min.css');
		$this->template->asset_js('scrollbar/jquery.scrollbar.min.js');
		$this->template->asset_css('scrollbar/jquery.scrollbar.css');
		$this->template->write_view('navbar', $this->folder_set.'navbar', $datos);
		$this->template->write_view('banner', $this->folder_set.'banner', $banner);
		$this->template->write_view('footer', $this->folder_set.'footer', $datos);
		$this->template->write_view('content', $this->folder.'/home/list', $datos);
		$this->template->render();
	}

	public function categoria($categoria = '')
	{
		$datos['class'] = $this->class;
		$banner['banner'] = 2;
		$datos['active'] = 'categoria';
		$this->template->write('title', 'Categoria');
		$this->template->write_view('navbar', $this->folder_set.'navbar', $datos);
		$this->template->write_view('banner', $this->folder_set.'banner', $banner);
		$this->template->write_view('footer', $this->folder_set.'footer', $datos);
		$this->template->write_view('content', $this->folder.'/categorias/list', $datos);
		$this->template->render();
	}

	public function info_producto($producto = '')
	{
		$datos['class'] = $this->class;
		$datos['active'] = 'producto';
		$this->template->write('title', 'Producto');
		$this->template->write_view('navbar', $this->folder_set.'navbar', $datos);
		$this->template->write_view('footer', $this->folder_set.'footer', $datos);
		$this->template->write_view('content', $this->folder.'/producto/list', $datos);
		$this->template->render();
	}

	public function bolsa_trabajo()
	{
		$datos['class'] = $this->class;
		$datos['active'] = 'bolsa';
		$this->template->write('title', 'Bolsa de Trabajo');
		$this->template->write_view('navbar', $this->folder_set.'navbar', $datos);
		$this->template->write_view('footer', $this->folder_set.'footer', $datos);
		$this->template->write_view('sucursales', $this->folder_set.'sucursales', $datos);
		$this->template->write_view('content', $this->folder.'/bolsa/list', $datos);
		$this->template->render();
	}

	public function socios()
	{
		$datos['class'] = $this->class;
		$datos['active'] = 'socios';
		$this->template->write('title', 'Socios');
		$this->template->write_view('navbar', $this->folder_set.'navbar', $datos);
		$this->template->write_view('footer', $this->folder_set.'footer', $datos);
		$this->template->write_view('sucursales', $this->folder_set.'sucursales', $datos);
		$this->template->write_view('content', $this->folder.'/socios/list', $datos);
		$this->template->render();
	}

	public function contacto()
	{
		$datos['class'] = $this->class;
		$datos['active'] = 'contacto';
		$this->template->write('title', 'Contacto');
		$this->template->write_view('navbar', $this->folder_set.'navbar', $datos);
		$this->template->write_view('footer', $this->folder_set.'footer', $datos);
		$this->template->write_view('content', $this->folder.'/contacto/list', $datos);
		$this->template->render();
	}

	public function carrito(){
		$datos['class'] = $this->class;
		$datos['active'] = 'carrito';
		$this->template->write('title', 'Carrito');
		$this->template->write_view('navbar', $this->folder_set.'navbar', $datos);
		$this->template->write_view('footer', $this->folder_set.'footer', $datos);
		$this->template->write_view('content', $this->folder.'/carrito/list', $datos);
		$this->template->render();
	}

	public function admin(){
		$this->load->view('categorias');
	}

}

/* End of file home.php */
/* Location: ./application/controllers/home.php */
