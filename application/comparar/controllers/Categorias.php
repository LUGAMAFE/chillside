<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Categorias extends CI_Controller {

    public function __construct()
    {
            parent::__construct();
            $this->load->model('Categorias_Model');
            $this->load->helper('url_helper');
    }

    public function home($url = ''){
        $grupos = @file_get_contents('http://slim.test/grupos');
        if($grupos == FALSE){
            show_404();
        }else{
            $grupos = json_decode($grupos, true);
            if($grupos["error"] == TRUE){
                show_404();
            }else{
                $grupos = $grupos["resultado"];
                $gruposBase = $this->Categorias_Model->getGrupos();
                $grupos = array_map(function($val) {
                    $val["nombre_grupo"] = ucwords($val["nombre_grupo"]);
                    return $val;
                },$grupos); 
                $diff = array_diff(array_column($grupos, 'nombre_grupo'), array_column($gruposBase, 'nombre_grupo'));
                $this->Categorias_Model->guardarGrupos($diff);
                
                $categorias = $this->Categorias_Model->getCategorias(FALSE);
                $categoriaPasada = null;
                $cont = -1;
                foreach ($categorias as $categoria) {
                    $nombreCategoria = $categoria["nombre_categoria"];
                    if($categoriaPasada === $nombreCategoria){
                        $data["categorias"][$cont]["grupos"][++$contGrupos] = array("nombre_grupo" => $categoria["nombre_grupo"], "id_grupo" => $categoria["id_grupo"]);
                    }else{
                        $cont++;
                        $contGrupos = 0;
                        $data["categorias"][$cont] = array(
                                "nombre_categoria" =>  $nombreCategoria,
                                "id-categoria" => $categoria["id_cat"],
                                "grupos" => array( $contGrupos => array("nombre_grupo" => $categoria["nombre_grupo"], "id_grupo" => $categoria["id_grupo"]))
                            );
                    }
                    $categoriaPasada = $nombreCategoria;
                }

                $data["gruposPendientes"] = $this->Categorias_Model->getGruposPendientes();
                $data["categoriasDesplegadas"] = 3;
                $data['class'] = $this->class;
                $this->template->write('title', 'Admin Home');
                $this->loadTemplatesComunes($data);
                
                $this->template->asset_css('custominputfile/css/jquery.Jcrop.min.css');
                $this->template->asset_css('custominputfile/css/custominputfile.min.css');
                $this->template->asset_js('assets/plugins/custominputfile/js/jquery.Jcrop.min.js');
                $this->template->asset_js('assets/plugins/custominputfile/js/custominputfile.js');

                $this->template->asset_css('jquerymodal/jquery.modal.min.css');
                $this->template->asset_js('jquerymodal/jquery.modal.min.js');

                $this->template->asset_css('scrollbar/jquery.scrollbar.css');
                $this->template->asset_js('scrollbar/jquery.scrollbar.min.js');

                $this->template->asset_js('sortablejs/Sortable.js');
                
                $this->template->asset_js('categorias.js');
                
                //$this->loadDataTables();
                $this->template->write_view('content', $this->folder.'/categorias/list', $data);
                $this->template->render();
            }
        }
    }

    public function gruposPendientesACategoria(){
        $grupos = $this->input->post('grupos');
        $categoria = $this->input->post('categoria');

        $error = $this->Categorias_Model->gruposPendientesACategoria($categoria, $grupos);
        $output = ["error" => !$error];

        $json = json_encode($output);
        echo $json;
    }

    public function grupoCategoriaAgruposPendientes(){
        $grupos = $this->input->post('grupos');
        $categoria = $this->input->post('categoria');

        $error = $this->Categorias_Model->grupoCategoriaAgruposPendientes($categoria, $grupos);
        $output = ["error" => !$error];

        $json = json_encode($output);
        echo $json;
    }

    public function grupoCategoriaAgrupoCategoria(){
        $grupos = $this->input->post('grupos');
        $categoriaOrigen = $this->input->post('categoriaOrigen');
        $categoriaDestino = $this->input->post('categoriaDestino');

        $error = $this->Categorias_Model->grupoCategoriaAgrupoCategoria($categoriaOrigen, $categoriaDestino, $grupos);
        $output = ["error" => !$error];

        $json = json_encode($output);
        echo $json;
    }

    public function nuevaCategoria(){
        $categoria = $this->input->post('categoria');
        $existeCategoria = $this->Categorias_Model->existeCategoriaNombre($categoria);
        if($existeCategoria){
            $output = ["error" => true, "respuesta" => "ya existe categoria"];
        }else{
            $insercion = $this->Categorias_Model->nuevaCategoria($categoria);
            if($insercion):
                $categoria = $this->Categorias_Model->getCategoria($categoria);
                ?>
                    <div class="categoria">
                        <div class="nombre">
                            <img class="handler svg" src="<?= site_url('assets/img/iconos/move-arrows.svg');?>" alt="">
                            <p class="nombreCategoria"><?=$categoria["nombre_categoria"]?></p>
                            <a href="#modalEditarCategoria"><img class="options svg" src="<?= site_url('assets/img/iconos/menu-bars.svg');?>" alt=""></a>
                        </div>
                        <ul id-categoria="<?=$categoria["id_cat"]?>" class="grupos scrollbar-inner contenedor-grupos"> 
                        </ul>
                    </div>
                <?php
                $respuesta = ob_get_contents();
                ob_clean();
                $output = ["error" => false, "respuesta" => $respuesta];
            else:
                $output = ["error" => true];
            endif;
        }
        $json = json_encode($output);
        echo $json;
    }

    public function eliminarCategoria(){
        $categoria = $this->input->post('categoria');
        $error = $this->Categorias_Model->eliminarCategoria($categoria);
        $output = ["error" => !$error];
        $json = json_encode($output);
        echo $json;
    }
}