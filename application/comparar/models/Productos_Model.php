<?php
class Productos_Model extends CI_Model {

    public function __construct(){
        parent::__construct("productos");
        $this->load->database();
    }

    public function getProductosInfo(){
        $this->db->select('id_mcp AS "Articulo", nombre_grupo_producto AS "Grupo"');
        $query = $this->db->get('productos');
        return $query->result_array();
    }

    public function insertarNuevosProductos(array $productos){
        if(count($productos) > 0){
            foreach ($productos as $producto) {
                $inserciones[] = array(
                    "id_mcp" => $producto["Articulo"],
                    "visible_prod" => 1,
                    "nombre_grupo_producto" => $producto["Grupo"]
                );
            }
            $resultado = $this->db->insert_batch('productos', $inserciones);
        }
    }

    public function actualizarGrupoProductos(array $productos){
        if(count($productos) > 0){
            foreach ($productos as $producto) {
                $update = array(
                    "nombre_grupo_producto" => $producto["Grupo"]
                );
                $this->db->where("id_mcp", $producto["Articulo"]);
                $result = $this->db->update('productos', $update);
            }
        }
    }

    public function actualizarVisibilidadProducto($idProducto, $visibilidad){
        $visibilidad = ($visibilidad === 'true' || $visibilidad === 'TRUE') ? true : false;
        if(!is_bool($visibilidad)){
            return false;
        }
        $visible = $visibilidad ? 1 : 0;
        $update = array(
            "visible_prod" =>  $visible
        );
        $this->db->where("id_mcp", $idProducto);
        $result = $this->db->update('productos', $update);
        return $result;
    }

    public function obtenerProductosTabla(){
        $this->db->select('id_prod, id_mcp, nombre_categoria, nombre_grupo, visible_prod');
        $this->db->from('productos');
        $this->db->join('grupos', 'productos.nombre_grupo_producto = grupos.nombre_grupo');
        $this->db->join('categorias_grupos', 'grupos.id_grupo = categorias_grupos.grupos_id_grupo', "left");
        $this->db->join('categorias', 'categorias_grupos.categorias_id_cat = categorias.id_cat', "left");
        $this->db->order_by("id_mcp", "ASC");
        $query = $this->db->get();  
        return $query->result_array();
    }

    
}