<?php
class Categorias_Model extends CI_Model {

    public function __construct(){
        $this->load->database();
    }

    public function guardarGrupos($grupos){
        foreach ($grupos as $grupo) {
            $dato = ["nombre_grupo" => ucwords($grupo)];
            
            $this->db->insert('grupos', $dato);
        }
    }

    // public function guardarGrupos($grupos){
    //     $grupos = array_map(function($val) {
    //         $val["nombre_grupo"] = ucwords($val["nombre_grupo"]);
    //         return $val;
    //     },$grupos); 

    //     $this->db->insert_batch('grupos',$grupos);
    // }

    public function getCategorias($categoria = FALSE){
        if ($categoria === FALSE){
            $this->db->select('id_cat, nombre_categoria, nombre_grupo, id_grupo');
            $this->db->from('categorias');
            $this->db->join('categorias_grupos', 'categorias.id_cat = categorias_grupos.categorias_id_cat', "left");
            $this->db->join('grupos', 'grupos.id_grupo = categorias_grupos.grupos_id_grupo', "left");
            $this->db->order_by("categorias_grupos.categorias_id_cat", "DESC");
            $query = $this->db->get();  
            return $query->result_array();
        }   

        $query = $this->db->get_where('nombre', array('nombre' => $categoria));
        return $query->row_array();
    }

    public function getGrupos(){
        $this->db->select('nombre_grupo');
        $this->db->from('grupos');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function getGruposPendientes(){
        $this->db->select('id_grupo, nombre_grupo');
        $this->db->from('grupos');
        $this->db->join('categorias_grupos', 'grupos.id_grupo = categorias_grupos.grupos_id_grupo', "left");
        $this->db->where("categorias_grupos.grupos_id_grupo IS null");
        $query = $this->db->get();
        return $query->result_array();
    }

    private function existeCategoria($categoria){
        $nombreCategoria = ucwords(strtolower($categoria["nombre"]));
        $idCat = $categoria["id"];
        $this->db->select('*');
        $this->db->from('categorias');
        $this->db->where("categorias.nombre_categoria = '$nombreCategoria' AND categorias.id_cat = $idCat");
        $query = $this->db->get();
        $existeCategoria = $query->num_rows();
        return $existeCategoria === 1 ? TRUE : FALSE;
    }

    public function existeCategoriaNombre($categoria){
        $nombreCategoria = ucwords(strtolower($categoria["nombre"]));
        $this->db->select('*');
        $this->db->from('categorias');
        $this->db->where("categorias.nombre_categoria = '$nombreCategoria'");
        $query = $this->db->get();
        $existeCategoria = $query->num_rows();
        return $existeCategoria === 1 ? TRUE : FALSE;
    }

    private function existeGrupo($grupo){
        $nombregrupo = $grupo["nombreGrupo"];
        $idgrupo = $grupo["idGrupo"];
        $this->db->select('*');
        $this->db->from('grupos');
        $this->db->where("grupos.nombre_grupo = '$nombregrupo' AND grupos.id_grupo = $idgrupo");
        $query = $this->db->get();
        $existegrupo = $query->num_rows();
        return $existegrupo === 1 ? TRUE : FALSE;
    }


    public function gruposPendientesACategoria($categoria, $grupos){
        $existeCategoria = $this->existeCategoria($categoria);
        if($existeCategoria){
            $idCategoria = $categoria["id"];
            foreach ($grupos as $grupo) {
                if(!$this->existeGrupo($grupo)){
                    return false;
                }else{
                    $data[] = array(
                            'categorias_id_cat' => $idCategoria,
                            'grupos_id_grupo' => $grupo["idGrupo"]
                    );
                }
            }
            $resultado = $this->db->insert_batch('categorias_grupos', $data);
            return $resultado === FALSE || $resultado <= 0 ? FALSE : TRUE;
        }else{
            return false;
        }
    }

    public function grupoCategoriaAgruposPendientes($categoria, $grupos){
        $existeCategoria = $this->existeCategoria($categoria);
        if($existeCategoria){
            $idCategoria = $categoria["id"];
            foreach ($grupos as $grupo) {
                if(!$this->existeGrupo($grupo)){
                    return false;
                }
            }
            $this->db->trans_start();
                foreach ($grupos as $grupo) {
                    $data = array(
                        'categorias_id_cat' => $idCategoria,
                        'grupos_id_grupo' => $grupo["idGrupo"]
                    );
                    $resultado = $this->db->delete('categorias_grupos', $data);
                    if(!$resultado){
                        break;
                    }
                }
            $this->db->trans_complete();
            return $resultado === FALSE ? FALSE : TRUE;
        }else{
            return false;
        }
    }

    public function grupoCategoriaAgrupoCategoria($categoriaOrigen, $categoriaDestino, $grupos){
        $existeCategoriaOrigen = $this->existeCategoria($categoriaOrigen);
        $existeCategoriaDestino = $this->existeCategoria($categoriaDestino);

        if($existeCategoriaOrigen && $existeCategoriaDestino){
            $idCategoriaOrigen = $categoriaOrigen["id"];
            $idCategoriaDestino = $categoriaDestino["id"];

            foreach ($grupos as $grupo) {
                if(!$this->existeGrupo($grupo)){
                    return false;
                }
            }

            $this->db->trans_start();
                foreach ($grupos as $grupo) {
                    $data = array(
                        'categorias_id_cat' => $idCategoriaOrigen,
                        'grupos_id_grupo' => $grupo["idGrupo"]
                    );
                    $resultado = $this->db->delete('categorias_grupos', $data);
                    if(!$resultado){
                        break;
                    }
                }
                if($resultado){
                    foreach ($grupos as $grupo) {
                        $data2[] = array(
                            'categorias_id_cat' => $idCategoriaDestino,
                            'grupos_id_grupo' => $grupo["idGrupo"]
                        );
                    }
                    $resultado = $this->db->insert_batch('categorias_grupos', $data2);
                }
            $this->db->trans_complete();
            return $resultado === FALSE || $resultado <= 0  ? FALSE : TRUE;
        }else{
            return false;
        }
    }

    public function nuevaCategoria($categoria){
        $data = array(
            'nombre_categoria' => ucwords(strtolower($categoria["nombre"])),
        );
        $resultado = $this->db->insert('categorias', $data);
        
        return $resultado;
    }

    public function eliminarCategoria($categoria){
        $existeCategoria = $this->existeCategoria($categoria);
        if(!$existeCategoria){
            return false;
        }

        $this->db->where('categorias_id_cat', $categoria["id"]);
        $resultado = $this->db->delete('categorias_grupos');
        
        $this->db->where('id_cat', $categoria["id"]);
        $resultado = $this->db->delete('categorias');
        return $resultado;
    }

    public function getCategoria($categoria){
        $data = array(
            'nombre_categoria' => $categoria["nombre"],
        );
        $query = $this->db->get_where('categorias', $data);
        $row = $query->row_array();
        return $row;
    }
}