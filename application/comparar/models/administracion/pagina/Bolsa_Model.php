<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bolsa_Model extends MY_Model {

	public function __construct(){
        parent::__construct("bolsa_page_data");	
    }

    public function guardarImagenBolsa($data, $update){
        if($update){
            $this->db->where($this->llave_primaria, 1);
            $resultado = $this->db->update($this->table, $data);
        }else{
            $resultado = $this->db->insert($this->table, $data);
        }
        return $resultado;
    }

    public function obtenerImagenBolsa(){
        return $this->getFirst();
    }
}    