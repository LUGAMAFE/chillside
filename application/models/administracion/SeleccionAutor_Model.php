<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SeleccionAutor_Model extends MY_Model {

	public function __construct(){
        parent::__construct("selecciones_autor");	
    }

    public function obtenerProductosRelacionados($listaIdsMcp){
        if($listaIdsMcp == null){
            $query = [];
        }else{
            $implodeIds = implode("','", $listaIdsMcp);
            $query = $this->db->query("select productos.id_prod AS id_prod, productos.id_mcp AS id_mcp, ifnull(categorias.nombre_categoria,'Sin Categorizar') AS nombre_categoria, subcategorias.nombre_subcategoria AS nombre_subcategoria, IF(productos.nombre_producto IS NULL or productos.nombre_producto = '', 'Sin Nombre', productos.nombre_producto) AS nombre_producto, productos.desc_mcp AS desc_mcp, (case when (productos.visible_prod = 1) then 'Visible' when (productos.visible_prod = 0) then 'Oculto' end) AS `visible_prod`, ifnull(archivos.full_route_file, 'assets/img/notfound.png') AS imagen_producto
            from productos join subcategorias on productos.subcategoria_mcp = subcategorias.nombre_subcategoria
            left join categorias_subcategorias on subcategorias.id_subcategoria = categorias_subcategorias.subcategorias_id_subcategoria
            left join categorias on categorias_subcategorias.categorias_id_cat = categorias.id_cat 
            left join imagenes_productos on imagenes_productos.id_producto_asoc_producto = productos.id_mcp AND imagenes_productos.tipo_imagen_producto = 'imagen_producto'
            left join archivos on imagenes_productos.id_file_asoc_producto = archivos.id_file
            WHERE id_mcp IN ('$implodeIds');");
            $query = $query->result_array();
        }
        return json_encode($query);
    }

    public function getImagenesSelecciones($id){
        $query = "SELECT archivos.* FROM archivos
        LEFT JOIN imagenes_ambiente_selecciones ON imagenes_ambiente_selecciones.id_file_asoc_amb = archivos.id_file 
        WHERE imagenes_ambiente_selecciones.id_selecciones_asoc = '$id'";
        $row = $this->query($query);
        return $row[0];
    }

    public function emptyImagenesAmbiente($id){
        $this->db->where('id_selecciones_asoc', $id);
        $this->db->delete('imagenes_ambiente_selecciones');
    }

    public function guardarImagenesAmbiente($id, $ids){
        foreach ($ids as $key => $value) {
            $datos = array(
                "id_selecciones_asoc" => $id,
                "id_file_asoc_amb" => $value,
            );
            $this->db->insert("imagenes_ambiente_selecciones", $datos);
        }
    }
}    