<?php
class Actualizacion_Model extends MY_Model {

    public function __construct(){
        parent::__construct("actualizacion");
        $this->load->database();
    }

    public function insert(){
        $datos = array(
            "fecha_modificacion_actualizacion" => date('Y/m/d H:i:s'),
        );
        $this->db->insert('actualizacion', $datos);
    }

    public function actualizar(){
        $this->db->select('*');
        $this->db->from('actualizacion');
        $this->db->where('id_actualizacion', 1);
        $query = $this->db->get();  
        $datos = array(
            "fecha_modificacion_actualizacion" => date('Y/m/d H:i:s'),
        );
        if($result = $query->result_array() != null){
            $this->db->where('id_actualizacion', 1);
            $this->db->update('actualizacion', $datos);
        } else {
            $this->db->insert('actualizacion', $datos);
        }
    }

    public function getUltimaFechaAct(){
        $this->db->select('fecha_modificacion_actualizacion');
        $this->db->from('actualizacion');
        $this->db->where('id_actualizacion', 1);
        $query = $this->db->get();
        $result = $query->result_array();
        if($result != null){
            return $result[0]["fecha_modificacion_actualizacion"];
        } else {
            return "";
        }
    }
}