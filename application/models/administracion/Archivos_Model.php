<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Archivos_Model extends MY_Model {

	public function __construct(){
        parent::__construct("archivos");	
        $this->load->database();
    }

    public function guardarArchivo($folder, $uuid, $name){
        $pathinfo = pathinfo($name);
        $ext = isset($pathinfo['extension']) ? $pathinfo['extension'] : '';
        $filename = isset($pathinfo['filename']) ? $pathinfo['filename'] : '';
        $folder = str_replace( "../public/" , "", $folder);

        $data = array(
            "uuid_file" => $uuid,
            "name_file" => $filename,
            "folder_file" => $folder,
            "ext_file" => $ext,
            "dir_file" => $folder . "/" . $uuid . "/",
            "full_route_file" => $folder . "/" . $uuid . "/" . $name,
            "fecha_creacion_file" => date('Y/m/d H:i:s'),
            "fecha_modificacion_file" => date('Y/m/d H:i:s'),
        );
        
        if($this->existeArchivo($uuid)){
            $data = array(
                "name_file" => $filename,
                "folder_file" => $folder,
                "ext_file" => $ext,
                "dir_file" => $folder . "/" . $uuid . "/",
                "full_route_file" => $folder . "/" . $uuid . "/" . $name,
                "fecha_creacion_file" => date('Y/m/d H:i:s'),
                "fecha_modificacion_file" => date('Y/m/d H:i:s'),
            );
            $resultado = $this->db->where('uuid_file', $uuid)->update('archivos', $data);
            $update_id = $this->obtenerIdPorArchivo($uuid);
            return $update_id;
        }else{
            $resultado = $this->db->insert('archivos', $data);
            $insert_id = $this->db->insert_id();
            return $insert_id;
        }
    }

    public function obtenerArchivoPorId($id){
        return $this->getById($id);
    }

    public function eliminarCarpetaArchivoPorId($id){
        $archivo = $this->obtenerArchivoPorId($id);
        $directorioValido = isInaccessible($archivo["folder_file"]);
        if($directorioValido){
            return false;
        }
        $target = $archivo["folder_file"].DIRECTORY_SEPARATOR.$archivo["uuid_file"];
        if (is_dir($target)){
            removeDir($target);
        }
        return true;
    }

    public function eliminarCarpetaArchivoPorUuid($uuid){
        $id = $this->obtenerIdPorArchivo($uuid);
        return $this->eliminarCarpetaArchivoPorId($id);
    }

    public function eliminarCarpetaArchivoPorCarpetaUuid($folder , $uuid){
        $folder = str_replace( "../public/" , "", $folder);
        $directorioValido = isInaccessible($folder);
        if($directorioValido){
            return false;
        }
        $target = $folder. $uuid;
        if (is_dir($target)){
            removeDir($target);
        }
        return true;
    }

    public function eliminarArchivoPorId($id){
        return $this->deleteById($id);
    }

    public function eliminarArchivoPorUuid($uuid){
        $id = $this->obtenerIdPorArchivo($uuid);
        return $this->eliminarArchivoPorId($id);
    }

    public function existeArchivo($uuid){
        $this->db->select('*');
        $this->db->from('archivos');
        $this->db->where("uuid_file = '$uuid'");
        $query = $this->db->get();
        $existe = $query->num_rows();
        return $existe === 1 ? TRUE : FALSE;
    }

    public function obtenerIdPorArchivo($uuid){
        $this->db->select('id_file');
        $this->db->from('archivos');
        $this->db->where("uuid_file = '$uuid'");
        $query = $this->db->get();
        $result = $query->row();
        return intval($result->id_file);
    }
}    