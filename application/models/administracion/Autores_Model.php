<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Autores_Model extends MY_Model {

	public function __construct(){
        parent::__construct("autores");	
        $this->load->database();
    }

    public function getImagenAutor($id){
        $query = "SELECT archivos.* FROM archivos
        LEFT JOIN autores ON autores.id_file_img_autor = archivos.id_file 
        WHERE autores.id_autor = '$id'";
        $row = $this->query($query);
        return $row[0];
    }

    public function getImagenesAmbienteAutor($id){
        $query = "SELECT archivos.* FROM archivos
        LEFT JOIN imagenes_ambiente_autores ON imagenes_ambiente_autores.id_file_asoc_amb = archivos.id_file 
        WHERE imagenes_ambiente_autores.id_autor_asoc = '$id'";
        $row = $this->query($query);
        return $row;
    }

    public function emptyImagenesAmbiente($id){
        $this->db->where('id_autor_asoc', $id);
        $this->db->delete('imagenes_ambiente_autores');
    }

    public function guardarImagenesAmbiente($id, $ids){
        foreach ($ids as $key => $value) {
            $datos = array(
                "id_autor_asoc" => $id,
                "id_file_asoc_amb" => $value,
            );
            $this->db->insert("imagenes_ambiente_autores", $datos);
        }
    }

    public function eliminarEntradaPorId($id){
        $this->db->where('id_autor', $id);
        $resultado = $this->db->delete('autores');
        return $resultado;
    }

    public function existeEntradaId($idEntrada){
        $this->db->select('*');
        $this->db->from('autores');
        $this->db->where("id_autor = '$idEntrada'");
        $query = $this->db->get();
        $existeCategoria = $query->num_rows();
        return $existeCategoria === 1 ? TRUE : FALSE;
    }

    public function existeEntrada($datos, $id = NULL){
        $nombre_autor = $datos["nombre"];
        $this->db->select('*');
        $this->db->from('autores');
        $this->db->where("id_autor='$id'");
        $query = $this->db->get();
        $existeCategoria = $query->num_rows();
        return $existeCategoria === 1 ? TRUE : FALSE;
    }

    public function obtenerEntradasTabla(){
        $this->db->select('*');
        $this->db->from('autores');
        $this->db->order_by("id_autor", "ASC");
        $query = $this->db->get();  
        return $query->result_array();
    }

    public function obtenerInfoEntradaModificarPorId($idEntrada){
        $this->db->select('*');
        $this->db->from('autores');
        $this->db->where("id_autor = '$idEntrada'");
        $query = $this->db->get();  
        return $query->row_array();
    }

    public function countentradas(){
        $this->db->select('COUNT(*)');
        $this->db->from('autores');
        $query = $this->db->get();
        $resultado = $query->row_array();
        return $resultado['COUNT(*)'];
    }

    public function resetEntradas(){
        $query = $this->db->query("ALTER TABLE autores AUTO_INCREMENT =  1");
    }

    public function obtenerSiguienteId(){
        $this->db->select('MAX(id_autor)');
        $this->db->from('autores');
        $query = $this->db->get();
        $resultado = $query->row_array();
        return isset($resultado['MAX(id_autor)']) ? $resultado['MAX(id_autor)'] + 1 : 1;
    }
}    