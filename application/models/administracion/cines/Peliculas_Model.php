<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Peliculas_Model extends MY_Model {

	public function __construct(){
        parent::__construct("peliculas_page_data");	
        $this->load->database();
    }

    //GET allCatalogos
    //trae el nombre de los catalogos 
    public function getAllCatalogos(){
        $query =$this->db->query("SELECT * FROM archivos LEFT JOIN  catalogos_page_data ON catalogos_page_data.id_file_img_catalogo = archivos.id_file");
        $catalogos = $query->result_array();  
        return $catalogos;
    }

    public function getImagenPelicula($id){
        $query = "SELECT archivos.* FROM archivos
        LEFT JOIN peliculas_page_data ON peliculas_page_data.id_file_img_pelicula = archivos.id_file 
        WHERE peliculas_page_data.id_pelicula = '$id'";
        $row = $this->query($query);
        return $row[0];
    }


    public function guardarPelicula($data, $update, $id = NULL){
        if($update){
            $this->db->where($this->llave_primaria, $id);
            $resultado = $this->db->update($this->table, $data);
        }else{
            $resultado = $this->db->insert($this->table, $data);
        }
        return $resultado;
    }

    public function eliminarEntradaPorId($id){
        $this->db->where('id_catalogo', $id);
        $resultado = $this->db->delete('catalogo');
        return $resultado;
    }

    public function existeEntradaId($idEntrada){
        $this->db->select('*');
        $this->db->from('catalogos_page_data');
        $this->db->where("id_catalogo = '$idEntrada'");
        $query = $this->db->get();
        $existeCategoria = $query->num_rows();
        return $existeCategoria === 1 ? TRUE : FALSE;
    }

    public function obtenerEntradasTabla(){
        $this->db->select('*');
        $this->db->from('catalogos_page_data');
        $this->db->order_by("id_catalogo", "ASC");
        $query = $this->db->get();  
        return $query->result_array();
    }

    public function obtenerInfoEntradaModificarPorId($idEntrada){
        $this->db->select('*');
        $this->db->from('catalogos_page_data');
        $this->db->where("id_catalogo = '$idEntrada'");
        $query = $this->db->get();  
        return $query->row_array();
    }

    public function countentradas(){
        $this->db->select('COUNT(*)');
        $this->db->from('catalogos_page_data');
        $query = $this->db->get();
        $resultado = $query->row_array();
        return $resultado['COUNT(*)'];
    }

    public function resetEntradas(){
        $query = $this->db->query("ALTER TABLE catalogos_page_data AUTO_INCREMENT =  1");
    }

}    