<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home_Model extends MY_Model {

	public function __construct(){
        parent::__construct("home_page_data");	
    }
    
    public function obtenerCodigoVideo(){
        $result = $this->getFirst();
        return is_null($result) ? $result : $result["id_file_video"];
    }

    public function actualizar($data){
        $this->ifExistUpdate(1, $data);
    }

    
}    