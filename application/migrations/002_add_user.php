<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Add_User extends CI_Migration {

	private $common_fields = array(
		"status int(1) DEFAULT 1",
		"creado datetime",
		"actualizado datetime DEFAULT '0000-00-00 00:00:00'",
		"borrado datetime DEFAULT NULL"
	);
	
	public function up()
	{
		$this->db->query('
			CREATE TABLE IF NOT EXISTS `Clientes` (
			  `id_cliente` INT NOT NULL AUTO_INCREMENT,
			  `nombres` VARCHAR(200) NULL,
			  `apellidos` VARCHAR(200) NULL,
			  `fecha_de_nacimiento` DATE NULL,
			  `metodo_registro` VARCHAR(50) NOT NULL,
			  `telefono` VARCHAR(15) NULL,
			  `email` VARCHAR(100) NOT NULL,
			  `contrasena` VARCHAR(500) NULL,
			  `salt` VARCHAR(30) NULL,
			  `token_activacion` TEXT NULL,
			  `token_user` TEXT NULL,
			  `fecha_ultimo_inicio_sesion` DATETIME NULL,
			  `estatus` int(1) DEFAULT \'1\',
			  `creado` datetime DEFAULT NULL,
			  `actualizado` datetime DEFAULT \'0000-00-00 00:00:00\',
			  `carrito_en_sesion` MEDIUMTEXT NULL,
			  PRIMARY KEY (`id_cliente`),
			  UNIQUE INDEX `id_cliente_UNIQUE` (`id_cliente` ASC))
			ENGINE = InnoDB;
		');

		$this->db->query('
			CREATE TABLE IF NOT EXISTS `DireccionesCliente` (
			  `id_direccion` INT NOT NULL AUTO_INCREMENT,
			  `identificador_direccion` VARCHAR(100) NOT NULL,
			  `calle` VARCHAR(100) NULL,
			  `numero_exterior` VARCHAR(50) NULL,
			  `numero_interior` VARCHAR(50) NULL,
			  `cruzamiento` VARCHAR(300) NULL,
			  `colonia` VARCHAR(200) NULL,
			  `codigo_postal` VARCHAR(50) NULL,
			  `ciudad` VARCHAR(200) NULL,
			  `estado` VARCHAR(100) NULL,
			  `pais` VARCHAR(100) NULL,
			  `longitud` VARCHAR(200) NULL,
			  `latitud` VARCHAR(200) NULL,
			  `estatus` int(1) DEFAULT \'1\',
			  `creado` datetime DEFAULT NULL,
			  `actualizado` datetime DEFAULT \'0000-00-00 00:00:00\',
			  `id_cliente` INT NOT NULL,
			  PRIMARY KEY (`id_direccion`),
			  UNIQUE INDEX `id_direccion_UNIQUE` (`id_direccion` ASC),
			  INDEX `fk_DireccionesCliente_Clientes1_idx` (`id_cliente` ASC),
			  CONSTRAINT `fk_DireccionesCliente_Clientes1`
			    FOREIGN KEY (`id_cliente`)
			    REFERENCES `Clientes` (`id_cliente`)
			    ON DELETE NO ACTION
			    ON UPDATE NO ACTION)
			ENGINE = InnoDB;
		');
	}

	public function down()
	{
		$this->dbforge->drop_table('DireccionesCliente');
		$this->dbforge->drop_table('Clientes');
	}

}

/* End of file 001_add_control.php */
/* Location: ./application/migrations/001_add_control.php */