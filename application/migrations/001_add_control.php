<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Migration_Add_Control extends CI_Migration {

	private $common_fields = array(
		"status int(1) DEFAULT 1",
		"creado datetime",
		"actualizado datetime DEFAULT '0000-00-00 00:00:00'",
		"borrado datetime DEFAULT NULL"
	);
	
	public function up()
	{
		$fields = array(
			"ip varchar(150)",
			"intentos integer NOT NULL",
			"bloqueo datetime NOT NULL",
		);

		$fields = array_merge($fields, $this->common_fields);

		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('ip', TRUE);
		$this->dbforge->create_table('direcciones_ip', TRUE);
		$this->db->simple_query('ALTER TABLE  `direcciones_ip` ENGINE=MyISAM DEFAULT CHARSET=utf8');

		$this->db->query('
			CREATE TABLE IF NOT EXISTS `ci_sessions` (
		        `id` varchar(40) NOT NULL,
		        `ip_address` varchar(45) NOT NULL,
		        `timestamp` int(10) unsigned DEFAULT 0 NOT NULL,
		        `data` blob NOT NULL,
		        KEY `ci_sessions_timestamp` (`timestamp`)
			);
		');
	}

	public function down()
	{
		$this->dbforge->drop_table('direcciones_ip');
		$this->dbforge->drop_table('ci_sessions');
	}

}

/* End of file 001_add_control.php */
/* Location: ./application/migrations/001_add_control.php */